<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atasan extends Model
{
    protected $table = 'atasan';
    protected $fillable = ['satuan','kepala','nip'];

    public function pegawai()
    {
    	return $this->hasMany(Pegawai::class);
    }
    
}
