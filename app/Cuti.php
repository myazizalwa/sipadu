<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $table = 'cuti';
    protected $fillable = ['jenis','alasan','dari','sampai','jumlah','alamat','notelp','status','keterangan','file_pengajuan','file_penerimaan','pegawai_id'];
    protected $dates = ['created_at'];

    public function pegawai()
    {
    	return $this->belongsTo(Pegawai::class);
    }

    public function getPengajuan()
    {
    	if (!$this->file_pengajuan) {
    		return asset('doc/no-pdf.jpg');
    	}
    	return asset('doc/pengajuan/'.$this->file_pengajuan);
    }

     public function getPenerimaan()
    {
    	if (!$this->file_penerimaan) {
    		return asset('doc/no-pdf.jpg');
    	}
    	return asset('doc/pengajuan/'.$this->file_penerimaan);
    }

    public function isicuti()
    {
        return 'isi cuti';
    }

    
}

