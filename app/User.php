<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pegawai()
    {
        return $this->hasOne(Pegawai::class);
    }
    
    public function absen()
    {
        return $this->hasMany(Absen::class);
    }

    public function libur()
    {
        return $this->hasMany(Libur::class);
    }
    
    public function isWeekend() {
        // $date = date("Y-m-d");
        $date = '2020-05-11';    
        return (date('N', strtotime($date)) >= 6); // bernilai 1 ketika akhir pekan
    }
    
    public function tanggalMerah() {
    
        // $array = json_decode(file_get_contents("https://sipadu.iainponorogo.ac.id/liburan.json"),true);
        $array = json_decode(file_get_contents("https://raw.githubusercontent.com/guangrei/Json-Indonesia-holidays/master/calendar.json"),true);
        
        $value = date("Ymd");
        // $value = "20200619";

        //check tanggal merah berdasarkan libur nasional
        if(isset($array[$value]))
        // :       echo"Libur ".$array[$value]["deskripsi"];
            :return "Libur ".$array[$value]["deskripsi"];

        //check tanggal merah berdasarkan weekend
        elseif(date("N",strtotime($value))== 7)

        // :       echo"tanggal merah hari minggu";
            :return 7;
        elseif(date("N",strtotime($value))== 6)

        // :       echo"tanggal merah hari Sabtu";
            :return 6;
        //bukan tanggal merah
        else
            // :echo"bukan tanggal merah";
            :return "hari aktif";
        endif;
    }
    
    public function tglIndo($date) {
        // $date = date('Y-m-d',strtotime($date));
        if($date == '0000-00-00')
            return 'Tanggal Kosong';
     
        $tgl = substr($date, 8, 2);
        $bln = substr($date, 5, 2);
        $thn = substr($date, 0, 4);
     
        switch ($bln) {
            case 1 : {
                    $bln = 'Januari';
                }break;
            case 2 : {
                    $bln = 'Februari';
                }break;
            case 3 : {
                    $bln = 'Maret';
                }break;
            case 4 : {
                    $bln = 'April';
                }break;
            case 5 : {
                    $bln = 'Mei';
                }break;
            case 6 : {
                    $bln = "Juni";
                }break;
            case 7 : {
                    $bln = 'Juli';
                }break;
            case 8 : {
                    $bln = 'Agustus';
                }break;
            case 9 : {
                    $bln = 'September';
                }break;
            case 10 : {
                    $bln = 'Oktober';
                }break;
            case 11 : {
                    $bln = 'November';
                }break;
            case 12 : {
                    $bln = 'Desember';
                }break;
            default: {
                    $bln = 'UnKnown';
                }break;
        }
     
        $hari = date('N', strtotime($date));
        switch ($hari) {
            case 7 : {
                    $hari = 'Minggu';
                }break;
            case 1 : {
                    $hari = 'Senin';
                }break;
            case 2 : {
                    $hari = 'Selasa';
                }break;
            case 3 : {
                    $hari = 'Rabu';
                }break;
            case 4 : {
                    $hari = 'Kamis';
                }break;
            case 5 : {
                    $hari = "Jum'at";
                }break;
            case 6 : {
                    $hari = 'Sabtu';
                }break;
            default: {
                    $hari = 'UnKnown';
                }break;
        }
     
        $tanggalIndonesia = $hari.", ".$tgl." " . $bln . " " . $thn;

        return $tanggalIndonesia;
    }
}
