<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Atasan;
use App\Jabatan;
use App\Pegawai;
use App\User;
use App\Cuti;


class PegawaiController extends Controller
{
    public function index(Request $request){

        if ($request->has('cari')) {
            $data_pegawai = Pegawai::where('nama','LIKE','%'.$request->cari.'%')->get();
        }else {
            $data_pegawai = Pegawai::orderBy('id','DESC')->get();
        }
    	
    	return view('pegawai.index',['data_pegawai' => $data_pegawai]);
    }

    public function tambah(Request $request){

        //panggil model users dan save ke tabel user
        $user = new User;
        $user->role = 'pegawai';
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->password = bcrypt('123');
        $user->remember_token = Str::random(60);
        $user->save();

        //insert table pegawai
        $request->request->add(['user_id' => $user->id]);
        $pegawai = Pegawai::create($request->all());

    	return redirect('/pegawai')->with('sukses','Data berhasil disimpan');
    } 

    public function edit($id){

    	$pegawai = Pegawai::find($id);
    	return view('pegawai/edit', ['pegawai' => $pegawai]);
    }

    public function update(Request $request,$id)
    {
        // dd($request->all());
    	$pegawai = Pegawai::find($id);
        //update ke table user
        if ($request->status == 'on') {
            $pegawai->user()->update([
                'status' => 1,
                'email' => $request->email
                ]);
        }
        else {
            $pegawai->user()->update([
              	'status' => 0,
                'email' => $request->email
                ]);
        }
            
    	$pegawai->update($request->all());
        if ($request->hasFile('avatar')) {
            $request->file('avatar')->move('profil/',$request->file('avatar')->getClientOriginalName());
            $pegawai->avatar = $request->file('avatar')->getClientOriginalName();
            $pegawai->save();
        }
    	return redirect('/pegawai')->with('sukses','Data berhasil diupdate');

    }
    
    public function reset($id){

    	$pegawai = Pegawai::find($id);
    	
    	$pegawai->user()->update([
    	    'password' => Hash::make('123')]);
    	    
    	return back()->with('sukses','Password berhasil direset');    
    	
    // 	return view('pegawai/edit', ['pegawai' => $pegawai]);
    }

    public function hapus($id){

        $pegawai = Pegawai::find($id);
        if ($pegawai->cuti() != NULL) {
            $pegawai->cuti()->delete();
        }

        if ($pegawai->user->absen() != NULL) {
            $pegawai->user->absen()->delete();
        }
        
        $pegawai->user()->delete();
        $pegawai->delete();
        
        
        // $user = User::find($userid);
        // $user->delete();

        return redirect('/pegawai')->with('sukses','Data berhasil dihapus');
    }

    public function profil($id)
    {
        $pegawai = Pegawai::find($id);
        return view('pegawai.profil',['pegawai' => $pegawai]);
    }

    public function addcuti(Request $request,$id)
    {
        //dd($request->all());

        $cuti = new Cuti;

        $cuti->jenis = $request->jenis;
        $cuti->dari = $request->date1;
        $cuti->sampai = $request->date;
        $cuti->jumlah = 2;
        $cuti->status = 'Pending';
        $cuti->pegawai_id = $id;

        $cuti->save();

        return redirect('/pegawai/'.$id.'/profil')->with('sukses','Pengajuan Cuti Berhasil');
        
    }

    public function profilsaya()
    {
        $pegawai = auth()->user()->pegawai;
        $atasan = Atasan::orderBy('satuan', 'ASC')->get();
        $jabatan = Jabatan::orderBy('jabatan', 'ASC')->get();

        return view('pegawai.profilsaya', compact(['pegawai','atasan','jabatan']));
    }

    public function updateUser(Request $request)
    {
        $this->validate ($request, [
            'nama' => 'required',
            'nip' => 'required|min:5',
            'atasan' => 'required',
            'jabatan' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'avatar' => 'mimes:jpg,jpeg,png|max:200'
        ]);
        
        // dd($request->all);
        //Update tabel user
        $user = auth()->user();
        $user->name = $request->nama;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->save();

        //Update tabel pegawai
        $id = auth()->user()->pegawai->id;
        $pegawai = auth()->user()->pegawai::find($id);

        // dd($request->all());
        $pegawai->nama = $request->nama;
        $pegawai->nip = $request->nip;
        $pegawai->atasan_id = $request->atasan;
        $pegawai->jabatan_id = $request->jabatan;
        $pegawai->alamat = $request->alamat;
        $pegawai->email = $request->email;
        $pegawai->tmt = $request->tmt;
        $pegawai->telp = $request->telp;

        $pegawai->save();

        // $pegawai->update($request->all());

        if ($request->hasFile('avatar')) {
            // $request->file('avatar')->move('profil/',$request->file('avatar')->getClientOriginalName());
            // $pegawai->avatar = $request->file('avatar')->getClientOriginalName();
            // $pegawai->save();

            $namafile = time().'.'.$request->file('avatar')->extension();
            //upload
            $request->file('avatar')->move('profil/',$namafile);
            //simpan ke db
            $pegawai->avatar = $namafile;
            $pegawai->save();
        }

        
        return redirect('/profilsaya')->with('sukses','Data berhasil diupdate');

    }
}
