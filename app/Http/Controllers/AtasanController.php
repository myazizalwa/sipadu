<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atasan;

class AtasanController extends Controller
{
    public function index()
    {
        $atasan = Atasan::where('id','!=',1)->paginate(10);

        return view('atasan.index',['atasan' => $atasan]);
    }

    public function add()
    {
    	return view('atasan.add');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate ($request, [
            'kepala' => 'required',
            'satuan' => 'required|unique:atasan'            
        ]);
		
		$atasan = Atasan::create($request->all());

        return redirect('/atasan')->with('sukses','Tambah Atasan Berhasil');
    }

    public function detil($id)
    {
        $atasan = Atasan::find($id);
        return view('atasan.detil', ['atasan' => $atasan]);
    }

    public function update(Request $request,$id)
    {
        
        $atasan = Atasan::find($id);
        // dd($atasan);
        $atasan->update($request->all());
        
        return redirect('/atasan')->with('sukses','Update Data Atasan Berhasil');
        
    }
    public function hapus($id){

        $atasan = Atasan::find($id);
        $atasan->delete();
        
        return back()->with('sukses', 'Hapus Atasan Berhasil');
    }
}
