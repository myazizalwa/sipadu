<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;
use \App\User;

class DashboardController extends Controller
{
    public function index(){
    	
    	$data_cuti = \App\Cuti::latest()->paginate(5);
    	$cuti = \App\Cuti::where('status', '=' , 'Usulan')->latest()->get();
    	$cutiditolak = \App\Cuti::where('status', '=' , 'Ditolak')->latest()->get();
    	$cutipending = \App\Cuti::where('status', '=' , 'Pending')->latest()->get();
    	$cutiditerima = \App\Cuti::where('status', '=' , 'Diterima')->latest()->get();

    	$pegawai = \App\Pegawai::all();
    	

    	return view('dashboard.index',['data_cuti' => $data_cuti, 'cuti' => $cuti, 'cutiditolak' => $cutiditolak, 'cutipending' => $cutipending, 'cutiditerima' => $cutiditerima, 'pegawai' => $pegawai]);
    }

    public function user()
    {
    	$pegawai = auth()->user()->pegawai;
        
        return view('dashboard.user', compact(['pegawai']));
    }

    public function pengelola()
    {
        $user = User::where('role','!=','pegawai')
                            ->get();

        return view('user.index',['user' => $user]);
    }

    public function tambah()
    {
                
        return view('user.add');
    }

    public function storeadmin(Request $request)
    {
        // dd($request->all());
        $this->validate ($request, [
            'nama' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required'            
        ]);

        $user = new User;
        
        $user->role = $request->role;
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->remember_token = Str::random(60);
        $user->save();

        return redirect('/pengelola')->with('sukses','Tambah Admin Berhasil');
    }

    public function detiladmin($id){

        $admin = User::find($id);
        // dd($admin);
        return view('user.detil', ['admin' => $admin]);
    }

    public function updateadmin(Request $request,$id){

        // dd($request->all());
        $admin = User::find($id);
        
        // $admin->update($request->all());
        
        $admin->role = $request->role;
        $admin->name = $request->nama;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->save();
        
        return redirect('/pengelola')->with('sukses','Update Data Admin Berhasil');
        
    }

    public function hapusadmin($id){

        $admin = User::find($id);
        $admin->delete();
        
        return back()->with('sukses', 'Hapus Admin Berhasil');
    }
}
