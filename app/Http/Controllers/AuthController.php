<?php

namespace App\Http\Controllers;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register()
    {
        return view('auths.register');
    }

    public function postregister(Request $request)
    {
       
        $this->validate ($request, [
            'nama' => 'required',
            'nip' => 'required|min:5|unique:pegawai',
            'email' => 'required|unique:pegawai',
            'password' => 'required'
        ]);
       //panggil model users dan save ke tabel user
        $user = new \App\User;

        $user->role = 'pegawai';
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->remember_token = Str::random(60);
        $user->save();

        //insert table pegawai
        $request->request->add(['user_id' => $user->id]);
        $pegawai = \App\Pegawai::create($request->all());

        return redirect('/login')->with('sukses','Selamat Anda Sukses Daftar di Sipadu');
    }

    public function login()
    {
    	return view('auths.login');
    }

    public function postlogin(Request $request)
    {
    	if (Auth::attempt($request->only('email','password'))) {
    		
            if (auth()->user()->role == 'admin') {
                return redirect('/dashboard');
            }elseif (auth()->user()->role == 'kepegawaian') {
                return redirect('/dashboard');
            }elseif (auth()->user()->role == 'keuangan') {
                return redirect('/dashboard');
            }
                return redirect('/user');
    	}
    		return redirect ('/login');
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect ('/login');
    }

    
}
