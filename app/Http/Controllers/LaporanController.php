<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Cuti;
use Carbon\Carbon;
use Auth;

use App\Pegawai;
use App\Absen;

class LaporanController extends Controller
{
    public function index()
    {
    	$data_cuti = Cuti::all();
    	
    	return view('laporan.all',['data_cuti' => $data_cuti]);
    }

    public function approve()
    {
    	$data_cuti = Cuti::where('status', '=' , 'Diterima')->latest()->get();
    	
    	return view('laporan.approve',['data_cuti' => $data_cuti]);
    }
    
    public function reject()
    {
        $data_reject = Cuti::where('status', '=' , 'Ditolak')->latest()->get();
        
        return view('laporan.reject',['data_reject' => $data_reject]);
    }

    public function pending()
    {
        $data_cuti = Cuti::where('status', '=' , 'Pending')->latest()->get();
        
        return view('laporan.pending',['data_cuti' => $data_cuti]);
    }

    public function usulan()
    {
        $data_cuti = Cuti::where('status', '=' , 'Usulan')->latest()->get();
        
        return view('laporan.usulan',['data_cuti' => $data_cuti]);
    }

    public function exportPdf($id)
    {
        $sekarang = Carbon::now();
        // echo $mytime->toDateTimeString();
        $cetakcuti = Cuti::find($id);

        $pdf = PDF::loadView('laporan.cetak.pdf', ['cetakcuti' => $cetakcuti, 'sekarang' => $sekarang]);
        return $pdf->download('lampiran-cuti.pdf');
    }

    //khusus pegawai login
    public function diterima()
    {
        $pegawai = auth()->user()->pegawai;
        $data_cuti = $pegawai->cuti();

        dd($data_cuti);
        
        //return view('laporan.approve',['data_cuti' => $data_cuti]);
    }
    
    public function absen($id)
    {
        $pegawai = Pegawai::find($id);
        $absen = $pegawai->user->absen;
        $user = Auth::user();


        //INISIASI 30 HARI RANGE SAAT INI JIKA HALAMAN PERTAMA KALI DI-LOAD
        //KITA GUNAKAN STARTOFMONTH UNTUK MENGAMBIL TANGGAL 1
        $start = Carbon::now()->startOfMonth()->format('Y-m-d');
        //DAN ENDOFMONTH UNTUK MENGAMBIL TANGGAL TERAKHIR DIBULAN YANG BERLAKU SAAT INI
        $end = Carbon::now()->endOfMonth()->format('Y-m-d');

        //JIKA USER MELAKUKAN FILTER MANUAL, MAKA PARAMETER DATE AKAN TERISI
        if (request()->date != '') {
            //MAKA FORMATTING TANGGALNYA BERDASARKAN FILTER USER
            $date = explode(' - ' ,request()->date);
            $start = Carbon::parse($date[0])->format('Y-m-d') . ' 00:00:01';
            $end = Carbon::parse($date[1])->format('Y-m-d') . ' 23:59:59';
        }
 
        $data = $absen->whereBetween('date',[$start, $end])->sortBy('date');
        
        return view('laporan.absen', compact(['absen', 'pegawai', 'data', 'user']));
    }

    public function absenPdf($id,$daterange)
    {

        $pegawai = Pegawai::find($id);
        $absen = $pegawai->user->absen;
        $user = Auth::user();

        // $user = Auth::user();
        // $user_id = Auth::user()->id;


        $date = explode('+', $daterange); //EXPLODE TANGGALNYA UNTUK MEMISAHKAN START & END
        //DEFINISIKAN VARIABLENYA DENGAN FORMAT TIMESTAMPS
        $start = Carbon::parse($date[0])->format('Y-m-d');
        $end = Carbon::parse($date[1])->format('Y-m-d');

        //KEMUDIAN BUAT QUERY BERDASARKAN RANGE CREATED_AT YANG TELAH DITETAPKAN RANGENYA DARI $START KE $END
        // $orders = Order::with(['customer.district'])->whereBetween('created_at', [$start, $end])->get();
        $presensi = $absen->whereBetween('date',[$start, $end]);
        // dd($presensi);

        //LOAD VIEW UNTUK PDFNYA DENGAN MENGIRIMKAN DATA DARI HASIL QUERY
        $pdf = PDF::loadView('laporan.absenadmin_pdf', compact('presensi', 'date','pegawai', 'user'));
        //GENERATE PDF-NYA
        return $pdf->stream();

    }
    
    public function presensi()
    {
        
        // $presensi = Absen::whereDate('date', Carbon::today())->get();
        $start = Carbon::now()->startOfWeek()->format('Y-m-d');
        //DAN ENDOFMONTH UNTUK MENGAMBIL TANGGAL TERAKHIR DIBULAN YANG BERLAKU SAAT INI
        $end = Carbon::now()->endOfWeek()->format('Y-m-d');

        //JIKA USER MELAKUKAN FILTER MANUAL, MAKA PARAMETER DATE AKAN TERISI
        if (request()->date != '') {
            //MAKA FORMATTING TANGGALNYA BERDASARKAN FILTER USER
            $date = explode(' - ' ,request()->date);
            $start = Carbon::parse($date[0])->format('Y-m-d');
            $end = Carbon::parse($date[1])->format('Y-m-d');
        }
 
        $presensi = Absen::whereBetween('date',[$start, $end])
          	       ->orderBy('date', 'desc')
                   ->get();
        
        return view('laporan.presensi', compact(['presensi']));
    }
    
}
