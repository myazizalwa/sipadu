<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jabatan;

class JabatanController extends Controller
{
    public function index()
    {
        $jabatan = Jabatan::where('id','!=',1)->get();

        return view('jabatan.index',['jabatan' => $jabatan]);
    }

    public function add()
    {
    	return view('jabatan.add');
    }

    public function store(Request $request)
    {
        $this->validate ($request, [
            'jabatan' => 'required|unique:jabatan'            
        ]);
		
		$jabatan = Jabatan::create($request->all());

        return redirect('/jabatan')->with('sukses','Tambah Jabatan Berhasil');
    }

    public function detil($id)
    {
        $jabatan = Jabatan::find($id);
        return view('jabatan.detil', ['jabatan' => $jabatan]);
    }

    public function update(Request $request,$id)
    {
        
        $jabatan = Jabatan::find($id);
        $jabatan->update($request->all());
        
        return redirect('/jabatan')->with('sukses','Update Data jabatan Berhasil');
    }

    public function hapus($id){

        $jabatan = Jabatan::find($id);
        $jabatan->delete();
        
        return back()->with('sukses', 'Hapus jabatan Berhasil');
    }
}
