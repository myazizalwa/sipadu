<?php

namespace App\Http\Controllers;

use App\Libur;
use Illuminate\Http\Request;

class LiburController extends Controller
{
    public function index()
    {
        $libur = Libur::latest()->get();
        // return $libur->toArray();
        // return $libur->toJson(JSON_PRETTY_PRINT);

        return view('libur.index',compact(['libur']));
    }
}
