<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use App\Notifications\NotifikasiCuti;
use DB;
use App\User;
use Carbon\Carbon;
use App\Cuti;


class CutiController extends Controller
{
    public function index()
    {
    	
        $yearNow = Carbon::now()->format('Y');
        $pegawai = auth()->user()->pegawai;

        
        $jumlahcuti = $pegawai->cuti->where('status', 'Diterima')
                                    ->sum('jumlah');
                                    // ->whereYear('created_at', $yearNow)
                                    // ->get();
        
        // dd($jumlahcuti);        

        return view('cuti.index', compact(['pegawai','jumlahcuti']));
    }

    public function tambahcuti(Request $request)
    {
        
        $fdate = $request->date1;
		$tdate = $request->date;
		$datetime1 = new DateTime($fdate);
		$datetime2 = new DateTime($tdate);
		$interval = $datetime1->diff($datetime2);
		$days = $interval->format('%a')+1;

		//dd($request->all());
		$id = auth()->user()->pegawai->id;

        $user = auth()->user();
        $cuti = new Cuti;

        $this->validate ($request, [
            'jenis' => 'required',
            'alasan' => 'required|min:5',
            'date1' => 'required',
            'date' => 'required',
            'alamat' => 'required',
            'notelp' => 'required',
            'pengajuan' => 'mimes:pdf|max:500'
        ]);
        
        //noitifikasi ke user sebelum save ke db
        // $user->notify(new NotifikasiCuti(User::findorFail(1)));

        $cuti->jenis = $request->jenis;
        $cuti->alasan = $request->alasan;
        $cuti->dari = $request->date1;
        $cuti->sampai = $request->date;
        $cuti->jumlah = $days;
        $cuti->alamat = $request->alamat;
        $cuti->notelp = $request->notelp;
        $cuti->status = 'Usulan';
        
        $cuti->pegawai_id = $id;

        $cuti->save();


        if ($request->hasFile('pengajuan')) {

            //memberi nama
            $namapdf = time().'.'.$request->file('pengajuan')->extension();
            //upload
            $request->file('pengajuan')->move('doc/pengajuan/',$namapdf);
            //simpan ke db
            $cuti->file_pengajuan = $namapdf;
            $cuti->save();
        }

        

        return redirect('/cuti')->with('sukses','Pengajuan Cuti Berhasil');
        
    }

    public function edit($id){

        $cuti = Cuti::find($id);
        return view('laporan/status/edit', ['cuti' => $cuti]);
    }

    public function update(Request $request,$id)
    {
        $this->validate ($request, [
            'keterangan' => 'required',
            'penerimaan' => 'mimes:pdf|max:500'
        ]);

        //dd($request->all());
        $cuti = Cuti::find($id);
        $cuti->update($request->all());


        if ($request->hasFile('penerimaan')) {
            
             //memberi nama
            $namafile = time().'.'.$request->file('penerimaan')->extension();
            //upload
            $request->file('penerimaan')->move('doc/penerimaan/',$namafile);
            //save
            $cuti->file_penerimaan = $namafile;
            $cuti->save();
        }

        return redirect('/laporan/'.$id.'/status')->with('sukses','Data berhasil diupdate');
    }

    public function editcuti($id){

        $cuti = Cuti::find($id);
        return view('cuti/edit', ['cuti' => $cuti]);
    }

    public function updatecuti(Request $request,$id)
    {
        $this->validate ($request, [
            'pengajuan' => 'mimes:pdf|max:500'
        ]);

        //dd($request->all());
        $cuti = Cuti::find($id);
        $cuti->update($request->all());


        if ($request->hasFile('pengajuan')) {
            
             //memberi nama
            $namafile = time().'.'.$request->file('pengajuan')->extension();
            //upload
            $request->file('pengajuan')->move('doc/pengajuan/',$namafile);
            //save
            $cuti->file_pengajuan = $namafile;
            $cuti->save();
        }

        return redirect('/profilsaya')->with('sukses','Data berhasil diupdate');
    }

    public function umum()
    {
        return view('cuti.umum');
    }
}
