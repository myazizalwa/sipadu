<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Absen;
use Auth;
use Carbon\Carbon;
use PDF;

class AbsenController extends Controller
{
    public function timeZone($location)
    {
        return date_default_timezone_set($location);
    }

    public function index()
    {
        $this->timeZone('Asia/Jakarta');
        $user_id = Auth::user()->id;
        $user = Auth::user();
        $date = date("Y-m-d");
        $time = date("H:i:s");
        //cek absen
        // $cek_absen = Absen::where(['date' => $date, 'user_id' => $user_id])
        //                         ->get()
        //                         ->first();
        $cek_absen = Auth::user()->absen()->where('date' , $date)
                                ->get()
                                ->first();                                 

        // dd($cek_absen);                                

        if ($time <= '06:00') {
            $info = array(
                "status" => "Kurang dari jam 06.00. Anda belum dapat mengisi presensi",
                "btnIn" => "disabled",
                "btnOut" => "disabled",
                "lkh" => "");
        } elseif ($time >= '20:30') {
            $info = array(    
                "status" => "Lebih dari jam 20.30. Anda tidak dapat mengisi presensi pulang",
                "btnIn" => "disabled",
                "btnOut" => "disabled",
                "lkh" => "");
        }elseif ($cek_absen == NULL) {
            $info = array(
                "status" => "Anda belum mengisi presensi",
                "btnIn" => "",
                "btnOut" => "disabled",
                "lkh" => "");

        } elseif ($cek_absen->time_pulang == NULL && $time > '12:00') {
            $info = array(
                "status" => "Jangan lupa presensi pulang dan LKH",
                "btnIn" => "disabled",
                "btnOut" => "",
                "lkh" => $cek_absen->lkh); 
        
        }elseif ($time <= '12:00' && $cek_absen->time_masuk != NULL) {
            $info = array(
                "status" => "Jangan lupa presensi pulang dan LKH",
                "btnIn" => "disabled",
                "btnOut" => "disabled",
                "lkh" => $cek_absen->lkh); 
        } else {
            $info = array(
                "status" => "Presensi hari ini telah selesai. Terimakasih",
                "btnIn" => "disabled",
                "btnOut" => "disabled",
                "lkh" => $cek_absen->lkh);
        }

        // return $info['status'];

        //INISIASI 30 HARI RANGE SAAT INI JIKA HALAMAN PERTAMA KALI DI-LOAD
        //KITA GUNAKAN STARTOFMONTH UNTUK MENGAMBIL TANGGAL 1
        $start = Carbon::now()->startOfMonth()->format('Y-m-d');
        //DAN ENDOFMONTH UNTUK MENGAMBIL TANGGAL TERAKHIR DIBULAN YANG BERLAKU SAAT INI
        $end = Carbon::now()->endOfMonth()->format('Y-m-d');

        //JIKA USER MELAKUKAN FILTER MANUAL, MAKA PARAMETER DATE AKAN TERISI
        if (request()->date != '') {
            //MAKA FORMATTING TANGGALNYA BERDASARKAN FILTER USER
            $date = explode(' - ' ,request()->date);
            $start = Carbon::parse($date[0])->format('Y-m-d') . ' 00:00:01';
            $end = Carbon::parse($date[1])->format('Y-m-d') . ' 23:59:59';
        }

        //BUAT QUERY KE DB MENGGUNAKAN WHEREBETWEEN DARI TANGGAL FILTER
        // $orders = Order::with(['customer.district'])->whereBetween('created_at', [$start, $end])->get();
 
        // $data = Absen::where('user_id' , $user_id)
        //             ->whereBetween('date',[$start, $end])
        //             ->orderBy('date' , 'desc')
        //             ->get();
        
        $data = Auth::user()->absen()->whereBetween('date',[$start, $end])
                            ->orderBy('date' , 'desc')
                            ->get();

        return view('absen.index', compact('data', 'info','time', 'user', 'date'));
    }

    public function absen(Request $request)
    {
        $this->timeZone('Asia/Jakarta');
        $user_id = Auth::user()->id;
        $date = date("Y-m-d"); //2020-3-2
        $time = date("H:i:s");
        $lkh = $request->lkh;

        $absen = new Absen;

        //Absen masuk
        if (isset($request["btnIn"])) {
            //cek double data
            $cek_double = $absen->where(['date' => $date, 'user_id' => $user_id])
                                ->count();

            if ($cek_double > 0) {
                return redirect()->back();
            }

            $absen->create([
                'user_id' => $user_id,
                'date' => $date,
                'time_masuk' => $time,
                'lkh' => $lkh]);

            return back()->with('sukses','Presensi kedatangan Berhasil');

        } 
        // Absen Pulang
        elseif (isset($request["btnOut"])) {

            $absen->where(['date' => $date, 'user_id' => $user_id])
                    ->update([
                        'time_pulang' => $time,
                        'lkh' => $lkh]);
            return back()->with('sukses','Presensi pulang Berhasil');
        }
        
    }
    
    // fungsi report absen
    public function absenReportPdf($daterange)
    {

        $user = Auth::user();
        $user_id = Auth::user()->id;
        $date = explode('+', $daterange); //EXPLODE TANGGALNYA UNTUK MEMISAHKAN START & END
        //DEFINISIKAN VARIABLENYA DENGAN FORMAT TIMESTAMPS
        $start = Carbon::parse($date[0])->format('Y-m-d');
        $end = Carbon::parse($date[1])->format('Y-m-d');

        //KEMUDIAN BUAT QUERY BERDASARKAN RANGE CREATED_AT YANG TELAH DITETAPKAN RANGENYA DARI $START KE $END
        // $orders = Order::with(['customer.district'])->whereBetween('created_at', [$start, $end])->get();
        $presensi = Absen::where('user_id' , $user_id)
                    ->whereBetween('date',[$start, $end])
                    ->orderBy('date' , 'asc')
                    ->get();
        //LOAD VIEW UNTUK PDFNYA DENGAN MENGIRIMKAN DATA DARI HASIL QUERY
        $pdf = PDF::loadView('laporan.absen_pdf', compact('presensi', 'date','user'));
        //GENERATE PDF-NYA
        return $pdf->stream();
    }
    
}
