<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absen extends Model
{
    protected $table = 'absen';
    protected $fillable = ['user_id','date','time_masuk','time_pulang','lkh'];
    
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
