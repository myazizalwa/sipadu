<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libur extends Model
{
    protected $table = 'libur';
    protected $fillable = ['user_id','date','deskripsi'];
    protected $visible = ['date', 'deskripsi'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
