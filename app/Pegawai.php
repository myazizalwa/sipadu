<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table='pegawai';
    //protected $dates = 'tmt';
    protected $fillable = ['nama','nip','satuan','jabatan','alamat','email','telp','avatar','tmt','user_id'];
    protected $dates = [
    'created_at',
    'updated_at',
    'tmt',
    // your other new column
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function cuti()
    {
    	return $this->hasMany(Cuti::class);
    }

    public function atasan()
    {
        return $this->belongsTo(Atasan::class);
    }

    public function jabatan()
    {
        return $this->belongsTo(Jabatan::class);
    }


    public function getAvatar()
    {
    	if (!$this->avatar) {
    		return asset('profil/default.png');
    	}
    	return asset('profil/'.$this->avatar);
    }

}

