@extends('layouts.master')

@section('title')
Form Absen - {{Auth::user()->name}}
@endsection

@section('scripts')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    //KETIKA PERTAMA KALI DI-LOAD MAKA TANGGAL NYA DI-SET TANGGAL SAA PERTAMA DAN TERAKHIR DARI BULAN SAAT INI
    $(document).ready(function() {
        let start = moment().startOf('month')
        let end = moment().endOf('month')

        //KEMUDIAN TOMBOL EXPORT PDF DI-SET URLNYA BERDASARKAN TGL TERSEBUT
        $('#exportpdf').attr('href', '/absen/pdf/' + start.format('YYYY-MM-DD') + '+' + end.format('YYYY-MM-DD'))

        //INISIASI DATERANGEPICKER
        $('#date').daterangepicker({
            startDate: start,
            endDate: end
        }, function(first, last) {
            //JIKA USER MENGUBAH VALUE, MANIPULASI LINK DARI EXPORT PDF
            $('#exportpdf').attr('href', '/absen/pdf/' + first.format('YYYY-MM-DD') + '+' + last.format('YYYY-MM-DD'))
        })
    })
</script>

@endsection

@section('content')
    <section class="section">
    <div class="section-header">
    <h1>SIPADU - Presensi Online Tanggap COVID-19</h1>
    </div>
    
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
      {{session('sukses')}}
    </div>
    @endif
        @if (Auth::user()->status == 1)
        {{-- Maaf anda tidak dapat absen melalui SIPADU --}}
        <div class="row">
            <div class="col-lg-6 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                    <h4><div class="badge badge-danger">
                        <strong>Tidak Bisa Menggunakan SIPADU</strong>
                        </div>
                    </h4>
                    </div>
                    <div class="card-body">
                            Mohon maaf, bapak/ibu tidak dapat absen melalui SIPADU. Silakan Absen di lingkungan kampus menggunakan mesin <strong>Finger Print</strong> 
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                    <h4>Keterangan Presensi</h4>
                    </div>
                    <div class="card-body">
                        - <strong>Presensi Masuk</strong> aktif mulai jam 06.00 WIB. Kurang dari jam tersebut tidak tersimpan</br>
                        - <strong>Presensi pulang</strong> mulai aktif mulai jam 12.00 WIB s/d 20.30 WIB dan sebelum klik presensi pulang, jangan lupa isi LKH anda hari ini. </br>
                        - Dinyatakan <div class="badge badge-danger"><strong>terlambat</strong></div>  apabila presensi lebih dari jam 07.30 WIB dan <div class="badge badge-warning"><strong>pulang awal</strong></div> apabila kurang dari jam 16.00 untuk hari jum'at 16.30
                    </div>
                </div>
          </div>

        </div>
        @else
        <div class="row">
            <!--Mulai cek tgl merah/akhir pekan-->
            @if(strpos($user->tanggalMerah(), 'Libur') !== false)
            {{-- {{$user->tanggalMerah()}} --}}

            <div class="col-lg-6 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                    <h4><div class="badge badge-danger">
                        <strong>{{$user->tanggalMerah()}}</strong>
                        </div>
                    </h4>
                    </div>
                    <div class="card-body">
                            Mohon maaf bapak/ibu, hari ini libur. 
                    </div>
            </div>
            @elseif($user->tanggalMerah() == 6)
            <div class="col-lg-6 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                    <h4><div class="badge badge-danger">
                        <strong>Libur Hari Sabtu</strong>
                        </div></h4>
                    </div>
                    <div class="card-body">
                            Mohon maaf bapak/ibu, hari ini libur. 
                    </div>
            </div>

            @elseif($user->tanggalMerah() == 7)
            <div class="col-lg-6 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                    <h4><div class="badge badge-danger">
                        <strong>Libur Hari Minggu</strong>
                        </div></h4>
                    </div>
                    <div class="card-body">
                            Mohon maaf bapak/ibu, hari ini libur. 
                    </div>
            </div>
            @else
            
            <div class="col-lg-6 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                    <h4>{{$info['status']}}</h4>
                    </div>
                    <div class="card-body">
                            <div class="form-group">
                                Waktu Server <strong>{{$time}}</strong>
                                {{auth()->user()->isWeekend()}}
                            </div>
                        <form action="/absen/masuk" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="d-flex justify-content-between">
                                    <button type="submit" class="btn btn-primary btn-large btn-icon icon-left" name="btnIn" {{$info['btnIn']}}>
                                        <span class="far fa-edit"></span>PRESENSI MASUK
                                    </button>
                                    <button type="submit" class="btn btn-success btn-large btn-icon icon-left" name="btnOut" {{$info['btnOut']}} onclick="return confirm('Anda sudah mengisi LKH?')">
                                        <span class="fas fa-check"></span>PRESENSI PULANG
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                              <textarea rows="5" class="form-control" placeholder="Isi LKH Anda Disini" name="lkh">{{$info['lkh']}}</textarea>
                            </div>
                          </div>
                        </form>
                </div>
            @endif
          </div>
          <div class="col-lg-6 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                    <h4>Keterangan Presensi</h4>
                    </div>
                    <div class="card-body">
                        - <strong>Presensi Masuk</strong> aktif mulai jam 06.00 WIB. Kurang dari jam tersebut tidak tersimpan</br>
                        - <strong>Presensi pulang</strong> mulai aktif mulai jam 12.00 WIB s/d 20.30 WIB dan sebelum klik presensi pulang, jangan lupa isi LKH anda hari ini. </br>
                        - Dinyatakan <div class="badge badge-danger"><strong>terlambat</strong></div>  apabila presensi lebih dari jam 07.30 WIB dan <div class="badge badge-warning"><strong>pulang awal</strong></div> apabila kurang dari jam 16.00 untuk hari jum'at 16.30
                    </div>
                </div>
          </div>
          
        </div> 
        
        @endif
        
        <!-- Menampilkan tabel riwayat -->
        <div class="row">
            <div class="col-lg-12 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                    <h4>Riwayat Presensi</h4>
                    </div>
                    <div class="card-body">
                        <form action="/absen" method="get">
                            <div class="input-group mb-3 col-md-4 float-right">
                                <input type="text" id="date" name="date" class="form-control">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">Filter</button>
                                </div>
                                <a target="_blank" class="btn btn-success" id="exportpdf">Cetak</a>
                            </div>
                        </form>
                        <div class="table-responsive">
                        <table class="table table-hover table-bordered"> 
                            <thead>
                                <th>Tanggal</th>
                                <th>Jam Masuk</th>
                                <th>Jam Pulang</th>
                                <th>LKH</th>
                            </thead>
                            <tbody>
                                @forelse ($data as $absen)
                                    <tr>
                                        <td >{{$user->tglIndo($absen->date)}}</td>
                                        @if($absen->time_masuk >= '08:00')
                                            <td><div class="badge badge-danger">{{ date('H:i', strtotime($absen->time_masuk)) }}</div></td>
                                        @else
                                             <td>{{ date('H:i', strtotime($absen->time_masuk)) }}</td>   
                                        @endif
                                        @if(is_null($absen->time_pulang))
                                            <td></td>
                                        @elseif($absen->time_pulang <= '15:00')
                                             <td><div class="badge badge-warning">{{ date('H:i', strtotime($absen->time_pulang)) }}</div></td>
                                        @else
                                            <td>{{ date('H:i', strtotime($absen->time_pulang)) }}</td>
                                        @endif
                                        <td>{{$absen->lkh}}</td>
                                    </tr>   
                                @empty
                                <tr>
                                    <td colspan="4"><center><b>Tidak Ada Data Presensi</b></center></td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        </div>
                        {{-- {{$data->links()}} --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop