@extends('layouts.master')

@section('title')
Form Pengajuan Cuti Umum
@endsection

@section('scripts')

<!-- Datepicker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css"/>

<!-- Datepicker -->
<script>
    $(document).ready(function(){
      var date_input=$('input[name="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>
<script>
    $(document).ready(function(){
      var date_input=$('input[name="date1"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>
@endsection

@section('content')
	<section class="section">
  	<div class="section-header">
    <h1>Selamat Datang <strong>{{auth()->user()->pegawai->nama}}</strong></h1>
	  	</div>
	        	<div class="row">
            <div class="col-lg-6 col-md-6 col-12">
            	<div class="card">
                  <div class="card-body">

                          <form action="/cuti/tambahcuti" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Jenis Cuti</label>
                          <select name="jenis" class="form-control {{$errors->has('jenis') ? 'has-error' : ''}}" id="exampleFormControlSelect1">
                            <option value="Sakit">Sakit</option>
                            <option value="Penting">Karena Alasan Penting</option>
                            <option value="Tanggungan">Diluar Tanggungan Negara</option>
                            <option value="Melahirkan">Melahirkan</option>
                            <option value="Besar">Cuti Besar</option>
                          </select>
                          @if($errors->has('jenis'))
                            <span class="help-block">{{$errors->first('jenis')}}</span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Alasan Cuti</label>
                          <textarea name="alasan" class="form-control {{$errors->has('alasan') ? 'has-error' : ''}}">{{old('alasan')}}</textarea>
                            @if($errors->has('alasan'))
                            <span class="help-block">{{$errors->first('alasan')}}</span>
                            @endif
                        </div>
                        <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label for="inputEmail4">Mulai</label>
                                  <input type="text" class="form-control datepicker {{$errors->has('date1') ? 'has-error' : ''}}" id="date1" name="date1" required="" value="{{old('date')}}">
                                  @if($errors->has('date1'))
                                  <span class="help-block">{{$errors->first('date1')}}</span>
                                  @endif
                                </div>
                                <div class="form-group col-md-6">
                                  <label for="inputPassword4">Sampai</label>
                                  <input type="text" class="form-control datepicker {{$errors->has('date') ? 'has-error' : ''}}" id="date" name="date" value="{{old('date')}}" required="">
                                  @if($errors->has('date'))
                                  <span class="help-block">{{$errors->first('date')}}</span>
                                  @endif
                                </div>
                          </div>
                          <div class="form-group">
                          <label for="exampleFormControlSelect1">Alamat Selama Cuti</label>
                          <textarea name="alamat" class="form-control {{$errors->has('alamat') ? 'has-error' : ''}}">{{old('alamat')}}</textarea>
                          @if($errors->has('alamat'))
                            <span class="help-block">{{$errors->first('alamat')}}</span>
                            @endif
                          </div>
                          <div class="form-group">
                          <label for="exampleFormControlSelect1">Nomor Telepon Selama Cuti</label>
                          <input type="number" class="form-control {{$errors->has('notelp') ? 'has-error' : ''}}" name="notelp" value="{{old('notelp')}}">
                          @if($errors->has('notelp'))
                            <span class="help-block">{{$errors->first('notelp')}}</span>
                            @endif
                          </div>
                          <div class="form-group">
                            <label for="pengajuan">Dokumen Pengajuan Cuti (maks 500 Kb pdf)</label>
                            <input type="file" class="form-control {{$errors->has('pengajuan') ? 'has-error' : ''}}" name="pengajuan">
                            @if($errors->has('pengajuan'))
                            <span class="help-block">{{$errors->first('pengajuan')}}</span>
                            @endif
                          </div>

                          <div class="modal-footer">
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </form>
                        </div>
                      </div>

                                       
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12">
              <div class="card">
                  <div class="card-body">
                    <div class="hero text-white hero-bg-image hero-bg-parallax" style="background-image: url('{{ asset('doc/splash.jpg') }}');">
                      <div class="hero-inner">
                        <h2>Welcome, <strong>{{auth()->user()->pegawai->nama}}</strong>!</h2>
                        <p class="lead">Cuti Umum Digunakan untuk bla bla bla<br>
                          Informasi lebih lengkap anda <br>dapat mendownload dokumen panduan dibawah ini.</p>
                        <div class="mt-4">
                          <a href=" {{ asset('doc/syarat.pdf') }} " class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="far fa-file"></i> Download Panduan Pengajuan Cuti</a>
                        </div>
                        <div class="mt-4">
                          <a href="#" class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="far fa-file"></i> Format Pengajuan Cuti</a>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
            </div>
          </div>
	    </div>
	</section>
@stop