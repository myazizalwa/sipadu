@extends('layouts.master')

@section('title')
Form Perubahan Pengajuan Cuti
@endsection

@section('scripts')

<!-- Datepicker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css"/>

<!-- Datepicker -->
<script>
    $(document).ready(function(){
      var date_input=$('input[name="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>
<script>
    $(document).ready(function(){
      var date_input=$('input[name="date1"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>
@endsection

@section('content')
	<section class="section">
  	<div class="section-header">
    <h1>Selamat Datang <strong>{{auth()->user()->pegawai->nama}}</strong></h1>
	  	</div>
	        	<div class="row">
            <div class="col-lg-6 col-md-6 col-12">
            	<div class="card">
                  <div class="card-body">
                    
                    @if (12- auth()->user()->pegawai->cuti->where('status', 'Diterima')->sum('jumlah') > 0)

                          <form action="/cuti/{{$cuti->id}}/update" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Jenis Cuti</label>
                          <select name="jenis" class="form-control" id="exampleFormControlSelect1">
                            <option value="Tahunan">Tahunan</option>
                            <option value="Umum">Umum</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Alasan Cuti</label>
                          <textarea name="alasan" class="form-control">{{$cuti->alasan}}</textarea>
                        </div>
                        <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label for="inputEmail4">Mulai</label>
                                  <input type="text" class="form-control datepicker" id="date1" name="date1" value="{{$cuti->dari}}">
                                </div>
                                <div class="form-group col-md-6">
                                  <label for="inputPassword4">Sampai</label>
                                  <input type="text" class="form-control datepicker" id="date" name="date" value="{{$cuti->sampai}}">
                                </div>
                          </div>
                          <div class="form-group">
                          <label for="exampleFormControlSelect1">Alamat Selama Cuti</label>
                          <textarea name="alamat" class="form-control">{{$cuti->alamat}}</textarea>
                          </div>
                          <div class="form-group">
                          <label for="exampleFormControlSelect1">Nomor Telepon Selama Cuti</label>
                          <input type="number" class="form-control" name="notelp" value="{{$cuti->notelp}}">
                          </div>
                          <div class="form-group">
                            <label for="pengajuan">Dokumen Pengajuan Cuti (maks 500 Kb pdf)</label>
                            <input type="file" class="form-control {{$errors->has('pengajuan') ? 'has-error' : ''}}" name="pengajuan">
                            @if($errors->has('pengajuan'))
                              <span class="help-block">{{$errors->first('pengajuan')}}</span>
                            @endif
                          </div>

                          <div class="modal-footer">
                          <button type="submit" class="btn btn-primary">Update</button>
                      </form>
                        </div>
                      </div>

                    @else

                    <div class="hero text-white hero-bg-image hero-bg-parallax" style="background-image: url('{{ asset('doc/splash.jpg') }}');">
                      <div class="hero-inner">
                        <h2>Mohon Maaf, <strong>{{auth()->user()->pegawai->nama}}</strong>!</h2>
                        <p class="lead">Anda Tidak Bisa Mengajukan Cuti</p>
                      </div>
                    </div>
                  </div>

                    @endif                    
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12">
              <div class="card">
                  <div class="card-body">
                    <div class="hero text-white hero-bg-image hero-bg-parallax" style="background-image: url('{{ asset('doc/splash.jpg') }}');">
                      <div class="hero-inner">
                        <h2>Alasan cuti, <strong>{{auth()->user()->pegawai->nama}}</strong> <u>{{$cuti->status}}</u></h2>
                        <p class="lead">Alasan : {{$cuti->keterangan}}
                        <div class="mt-4">
                          <a href=" {{ asset('doc/syarat.pdf') }} " class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="far fa-file"></i> Download File Pengajuan Cuti</a>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
            </div>
          </div>
	    </div>
	</section>
@stop