@extends('layouts.master')

@section('title')
Dashboard {{$pegawai->nama}}
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Profile Lengkap {{$pegawai->nama}}</h1>
  </div>
  <div class="section-body">

            @if ( $pegawai->telp == null)
            <div class="col-12 mb-4">
                <div class="hero text-white hero-bg-image" style="background-image: url('{{ asset('doc/splash.jpg') }}');">
                  <div class="hero-inner">
                    <h2>Selamat Datang, {{$pegawai->nama}}</h2>
                    <p class="lead">Anda hampir selesai, lengkapi informasi mengenai diri anda untuk dapat menggunakan aplikasi <b>Sipadu</b>.<br>
                      Data Kurang : <br>
                      @if($pegawai->atasan_id == null)
                      - Atasan <br> 
                      @endif
                      @if($pegawai->jabatan_id == null)
                      - Jabatan <br> 
                      @endif
                      @if($pegawai->alamat == null)
                      - Alamat <br> 
                      @endif
                      @if($pegawai->telp == null)
                      - Nomor Telepon <br> 
                      @endif
                      @if($pegawai->tmt == null)
                      - Tertanggal Masuk (TMT) <br> 
                      @endif
                    </p>
                    <div class="mt-4">
                      <a href="/profilsaya" class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="far fa-user"></i> Lengkapi Profil</a>
                    </div>
                  </div>
                </div>
            </div>
            @else
            <div class="col-12 mb-4">
                <div class="hero text-white hero-bg-image" style="background-image: url('{{ asset('doc/splash.jpg') }}');">
                  <div class="hero-inner">
                    <h2>Selamat Datang, {{$pegawai->nama}}</h2>
                    <p class="lead">Selamat menggunakan aplikasi <b>Sipadu</b>.<br>
                      
                    </p>
                    <div class="mt-4">
                      <a href="/absen" class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="fa fa-users"></i> Presensi</a>
                      <a href="/profilsaya" class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="far fa-user"></i> Cek Profil</a>
                    </div>
                  </div>
                </div>
            </div>
            @endif
        <!--    <h2 class="section-title">Overview</h2>-->
        <!--    <p class="section-lead">-->
        <!--      Semua Informasi Mengenai Akun Anda-->
        <!--    </p>-->
        <!--    <div class="card">-->
        <!--      <div class="card-body">-->
                  
                  <!-- Di hide sementara -->
        <!--        <div class="row">-->
        <!--          <div class="col-lg-6">-->
        <!--            <div class="card card-large-icons">-->
        <!--              <div class="card-icon bg-primary text-white">-->
        <!--                <i class="fas fa-cog"></i>-->
        <!--              </div>-->
        <!--              <div class="card-body">-->
        <!--                <h4>Cuti Tahunan</h4>-->
        <!--                <p>Cuti Tahunan digunakan untuk cuti dengan kategori seperti Cuti Urusan Keluarga, Liburan dll.</p>-->
        <!--                <a href="/cuti" class="btn btn-success">Ajukan Cuti <i class="fas fa-chevron-right"></i></a>-->
        <!--              </div>-->
        <!--            </div>-->
        <!--          </div>-->
        <!--          <div class="col-lg-6">-->
        <!--            <div class="card card-large-icons">-->
        <!--              <div class="card-icon bg-success text-white">-->
        <!--                <i class="fas fa-users"></i>-->
        <!--              </div>-->
        <!--              <div class="card-body">-->
        <!--                <h4>Cuti Umum</h4>-->
        <!--                <p>Cuti Umum digunakan untuk cuti seperti Sakit, Haji/Umroh, Cuti Besar dll.</p>-->
        <!--                <a href="/cuti/umum" class="btn btn-success">Ajukan Cuti <i class="fas fa-chevron-right"></i></a>-->
        <!--              </div>-->
        <!--            </div>-->
        <!--          </div>-->
        <!--        </div>-->
                
                <!--usai-->
                
                <!--History Cuti-->
        <!--<div class="row">-->
        <!--    <div class="col-md-12">-->
        <!--      <div class="card">-->
        <!--        <div class="card-header">-->
        <!--          <h4>History Cuti Anda</h4>-->
        <!--        </div>-->
        <!--        <div class="card-body p-0">-->
        <!--          <div class="table-responsive table-invoice">-->
        <!--            <table class="table table-striped">-->
        <!--              <tbody><tr>-->
        <!--                <th>Alasan</th>-->
        <!--                <th>Periode</th>-->
        <!--                <th>Keterangan</th>-->
        <!--                <th>Status</th>-->
        <!--                <th>Action</th>-->
        <!--              </tr>-->
        <!--              @foreach($pegawai->cuti as $cuti)-->
        <!--                <tr>-->
        <!--                              <td><strong>({{ $cuti->jenis}})</strong> - {{ $cuti->alasan}}</td>-->
        <!--                              <td>{{$cuti->dari}} <strong>sampai</strong> {{$cuti->sampai}} </td>-->
        <!--                              <td>{{ $cuti->keterangan}}</td>-->
        <!--                              @if ($cuti->status == "Diterima")-->
        <!--                                <td><label class="badge badge-success">{{$cuti->status}}</label></td>-->
        <!--                                <td>-->
        <!--                                  <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Cetak" href="/laporan/{{ $cuti->id}}/exportPdf"><i class="fa fa-print"></i></a>-->
        <!--                                  <a class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Dokumen Cuti"><i class="far fa-file"></i></a>-->
        <!--                                </td>-->
        <!--                              @elseif($cuti->status == "Ditolak")-->
        <!--                                <td><label class="badge badge-danger">{{$cuti->status}}</label></td>-->
        <!--                              @elseif($cuti->status == "Usulan")-->
        <!--                                <td><label class="badge badge-info">{{$cuti->status}}</label></td>-->
        <!--                                <td>-->
        <!--                                  <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Cetak" href="/laporan/{{ $cuti->id}}/exportPdf"><i class="fa fa-print"></i></a>-->
        <!--                                </td>-->
        <!--                              @else-->
        <!--                                <td><label class="badge badge-warning"><a href="/cuti/{{ $cuti->id}}/edit">{{$cuti->status}}</a></label>-->
        <!--                                </td>-->
        <!--                              @endif-->
        <!--                            </tr>-->
        <!--               @endforeach-->
        <!--               <tr>-->
        <!--                 <td colspan="4"><center><b>Tidak ada Data Cuti</b></center>-->
        <!--                 </td>-->
        <!--               </tr>            -->
        <!--            </tbody>-->
        <!--            </table>-->
        <!--          </div>-->
        <!--        </div>-->
        <!--      </div>-->
        <!--    </div>-->
        <!--</div>-->
        
        <!--End History Cuti-->
        <!--      </div>-->
        <!--    </div>-->
          </div>
</section>
@stop