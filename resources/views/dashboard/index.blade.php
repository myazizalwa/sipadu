@extends('layouts.master')

@section('title')
Dashboard
@endsection

@section('content')
	<section class="section">
  	<div class="section-header">
    <h1>Selamat Datang <strong>{{auth()->user()->name}}</strong></h1>
	  	</div>
		  	<div class="section-body">
		      <div class="row">
	            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
	              <div class="card card-statistic-1">
	                <div class="card-icon bg-primary">
	                  <i class="far fa-user"></i>
	                </div>
	                <div class="card-wrap">
	                  <div class="card-header">
	                    <h4>Total Pegawai</h4>
	                  </div>
	                  <div class="card-body">
	                    {{$pegawai->count()}}
	                  </div>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
	              <div class="card card-statistic-1">
	                <div class="card-icon bg-danger">
	                  <i class="far fa-newspaper"></i>
	                </div>
	                <div class="card-wrap">
	                  <div class="card-header">
	                    <h4>Cuti Ditolak</h4>
	                  </div>
	                  <div class="card-body">
	                    {{$cutiditolak->count()}}
	                  </div>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
	              <div class="card card-statistic-1">
	                <div class="card-icon bg-warning">
	                  <i class="far fa-file"></i>
	                </div>
	                <div class="card-wrap">
	                  <div class="card-header">
	                    <h4>Cuti Pending</h4>
	                  </div>
	                  <div class="card-body">
	                    {{$cutipending->count()}}
	                  </div>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
	              <div class="card card-statistic-1">
	                <div class="card-icon bg-success">
	                  <i class="fas fa-circle"></i>
	                </div>
	                <div class="card-wrap">
	                  <div class="card-header">
	                    <h4>Cuti Diterima</h4>
	                  </div>
	                  <div class="card-body">
	                    {{$cutiditerima->count()}}
	                  </div>
	                </div>
	              </div>
	            </div>  

	        </div>

	        <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header">
                  <h4>Laporan Semua Cuti</h4>
                  <div class="card-header-action">
                    <a href="/laporan" class="btn btn-danger">View More <i class="fas fa-chevron-right"></i></a>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="table-responsive table-invoice">
                    <table class="table table-striped">
                      <tbody><tr>
                        <th>Nama</th>
                        <th>Jenis Cuti</th>
                        <th>Status</th>
                        <th>Periode</th>
                        <th>Aksi</th>
                      </tr>
                       @foreach($data_cuti as $data)
                      <tr>
                        <td class="font-weight-600">{{$data->pegawai->nama}}</td>
                        <td class="font-weight-600">{{$data->jenis}}</td>
                        @if ($data->status == "Diterima")
                          <td><div class="badge badge-success">{{$data->status}}</div></td>
                        @elseif($data->status == "Ditolak")
                          <td><div class="badge badge-danger">{{$data->status}}</div></td>
                        @elseif($data->status == "Pending")
                          <td><div class="badge badge-warning">{{$data->status}}</div></td>
                        @else
                          <td><div class="badge badge-info">{{$data->status}}</div></td>
                        @endif
                        <td>{{$data->dari}} s/d {{$data->sampai}}</td>
                        <td>
                          <a href="/laporan/{{$data->id}}/status" class="btn btn-primary">Detail</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody></table>
                  </div>
                  <div class="card-footer text-left">
                    <nav class="d-inline-block">
                          {{ $data_cuti->links() }}
                    </nav>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-hero">
                <div class="card-header">
                  <div class="card-icon">
                    <i class="far fa-question-circle"></i>
                  </div>
                  <h4>{{$cuti->count()}}</h4>
                  <div class="card-description">Pengajuan Cuti Baru</div>
                </div>
                <div class="card-body p-0">
                  <div class="tickets-list">

                    @foreach($cuti as $usulan)
                    <a href="/laporan/{{$usulan->id}}/status" class="ticket-item">
                      <div class="ticket-title">
                        <h4>Pengajuan Baru dari <span class="badge badge-light">{{ $usulan->pegawai->nama}}</span></h4>
                      </div>
                      <div class="ticket-info">
                        <div class="text-primary">{{ $usulan->created_at->diffForHumans()}}</div>
                      </div>
                    </a>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
	    </div>
	</section>
@stop