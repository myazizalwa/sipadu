<aside id="sidebar-wrapper">
  <div class="sidebar-brand">
    <a href="#">{{ env('APP_NAME') }}</a>
  </div>
  <div class="sidebar-brand sidebar-brand-sm">
    <a href="index.html">St</a>
  </div>
  <ul class="sidebar-menu">
      @if(auth()->user()->role == 'admin')
      <li class="menu-header">Dashboard</li>
      <li class="#"><a class="nav-link" href="/dashboard"><i class="fa fa-columns"></i> <span>Dashboard</span></a></li>
      <li class="menu-header">Administrasi</li>
      <li class="#"><a class="nav-link" href="/pengelola"><i class="fa fa-user"></i> <span>Admin</span></a></li>
      <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-file-alt"></i> <span>Administrasi</span></a>
              <ul class="dropdown-menu" style="display: none;">
                <li><a class="nav-link" href="/pegawai">Pegawai</a></li>
                <li><a class="nav-link" href="/atasan">Atasan</a></li>
                <li><a class="nav-link" href="/jabatan">Jabatan</a></li>
                <li><a class="nav-link" href="/libur">Hari Libur</a></li>
              </ul>
      </li>
      <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-file-alt"></i> <span>Laporan</span></a>
              <ul class="dropdown-menu" style="display: none;">
                <li><a class="nav-link" href="/laporan">Semua Cuti</a></li>
                <li><a class="nav-link" href="/laporan/usulan">Diusulkan</a></li>
                <li><a class="nav-link" href="/laporan/approve">Disetujui</a></li>
                <li><a class="nav-link" href="/laporan/reject">Ditolak</a></li>
                <li><a class="nav-link" href="/laporan/pending">Ditangguhkan</a></li>
              </ul>
        </li>
      @endif
      @if(auth()->user()->role == 'kepegawaian')
      <li class="menu-header">Dashboard</li>
      <li class="#"><a class="nav-link" href="/dashboard"><i class="fa fa-columns"></i> <span>Dashboard</span></a></li>
      <li class="menu-header">Administrasi</li>
      <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-file-alt"></i> <span>Administrasi</span></a>
              <ul class="dropdown-menu" style="display: none;">
                <li><a class="nav-link" href="/pegawai">Pegawai</a></li>
                <li><a class="nav-link" href="/atasan">Atasan</a></li>
                <li><a class="nav-link" href="/jabatan">Jabatan</a></li>
                <li><a class="nav-link" href="/libur">Hari Libur</a></li>
              </ul>
      </li>
      <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-file-alt"></i> <span>Laporan</span></a>
              <ul class="dropdown-menu" style="display: none;">
                <li><a class="nav-link" href="/laporan/presensi">Laporan Presensi</a></li>
                <li><a class="nav-link" href="/laporan">Semua Cuti</a></li>
                <li><a class="nav-link" href="/laporan/usulan">Diusulkan</a></li>
                <li><a class="nav-link" href="/laporan/approve">Disetujui</a></li>
                <li><a class="nav-link" href="/laporan/reject">Ditolak</a></li>
                <li><a class="nav-link" href="/laporan/pending">Ditangguhkan</a></li>
              </ul>
        </li>
      @endif
      @if(auth()->user()->role == 'keuangan')
      <li class="menu-header">Dashboard</li>
      <li class="#"><a class="nav-link" href="/dashboard"><i class="fa fa-columns"></i> <span>Dashboard</span></a></li>
      <li class="menu-header">Administrasi</li>
      @endif
      @if(auth()->user()->role == 'pegawai')
      <li class="menu-header">Dasboard</li>
      <li><a class="nav-link" href="/user"><i class="fa fa-columns"></i> <span>Dashboard</span></a></li>
      <li><a class="nav-link" href="/profilsaya"><i class="fa fa-users"></i> <span>Profil</span></a></li>
      <li><a class="nav-link" href="/absen"><i class="fa fa-users"></i> <span>Presensi</span></a></li>
      <!--<li class="dropdown">-->
      <!--        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-file-alt"></i> <span>Pengajuan Cuti</span></a>-->
      <!--        <ul class="dropdown-menu" style="display: none;">-->
      <!--          <li><a class="nav-link" href="/cuti">Tahunan</a></li>-->
      <!--          <li><a class="nav-link" href="/cuti/umum">Umum</a></li>-->
      <!--        </ul>-->
      <!--  </li>-->
        <li><a class="nav-link" href="doc/panduan.pdf"><i class="fa fa-file-alt"></i> <span>Panduan</span></a></li>
      @endif
    </ul>
</aside>