@extends('layouts.master')

@section('title')
Pengelolaan Hari Libur
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Pengelolaan Hari Libur</h1>
  </div>
<div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4>List Hari Libur</h4>
                  <div class="card-header-action">
                    <a href="#" class="btn btn-success">Tambah Hari Libur <i class="fas fa-chevron-right"></i></a>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="table-responsive table-invoice">
                    <table class="table table-striped">
                      <tbody><tr>
                        <th>No</th>
                        <th>Hari, Tanggal</th>
                        <th>Deskripsi</th>
                        <th>Dibuat Oleh</th>
                        <th>Aksi</th>
                      </tr>
                      @foreach($libur as $liburan)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td class="font-weight-600">{{auth()->user()->tglIndo($liburan->date)}}</td>
                        <td class="font-weight-600">{{$liburan->deskripsi}}</td>
                        <td class="font-weight-600">{{$liburan->user->name}}</td>
                        <td>
                          <a href="#" class="btn btn-danger">Hapus</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody></table>
                  </div>
                  <div class="card-footer text-left">
                    <nav class="d-inline-block">
                          {{-- {{ $atasan->links() }} --}}
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
</section>
@stop