@extends('layouts.master')

@section('title')
Pengelolaan Jabatan Langsung
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Pengelolaan Jabatan Langsung</h1>
  </div>
<div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4>List Jabatan</h4>
                  <div class="card-header-action">
                    <a href="/jabatan/tambah" class="btn btn-success">Tambah Jabatan <i class="fas fa-chevron-right"></i></a>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="table-responsive table-invoice">
                    <table class="table table-striped">
                      <tbody><tr>
                        <th>Jabatan</th>
                        <th>Action</th>
                      </tr>
                      @foreach($jabatan as $jabat)
                      <tr>
                        <td><a href="/jabatan/{{$jabat->id}}/detil">{{$jabat->jabatan}}</a></td>
                        <td>
                          <a href="/jabatan/{{$jabat->id}}/detil" class="btn btn-primary">Detail</a>
                          <a href="/jabatan/{{$jabat->id}}/hapus" class="btn btn-danger">Hapus</a>
                        </td>
                        
                      </tr>
                      @endforeach
                    </tbody></table>
                  </div>
                </div>
              </div>
            </div>
          </div>
</section>
@stop