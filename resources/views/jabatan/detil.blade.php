@extends('layouts.master')

@section('title')
Form Update Jabatan
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Form Update Jabatan</h1>
  </div>

	<div class="card">
		<div class="card-header"><h4>Update Jabatan</h4>
		</div> 
			<div class="card-body">
				<form action="/jabatan/{{$jabatan->id}}/update" method="POST" class="needs-validation" novalidate="">
                        {{csrf_field()}} 
					<div class="form-group row mb-4">
						<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Jabatan</label> 
						<div class="col-sm-12 col-md-6">
							<input type="text" placeholder="Nama Lengkap Jabatan" class="form-control {{$errors->has('jabatan') ? 'has-error' : ''}}" name="jabatan" required="" value="{{$jabatan->jabatan}}"> 
							<div class="invalid-feedback">
                              Mohon isi nama lengkap
                            </div>
                            @if($errors->has('jabatan'))
		                        <span class="help-block">{{$errors->first('jabatan')}}</span>
		                    @endif
						</div>
					</div>
					
					<div class="form-group row mb-4">
						<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label> 
						<div class="col-sm-12 col-md-7">
							<button class="btn btn-primary"><span>Update</span></button>
						</div>
					</div>
				</form>
			</div>
		</div>
</section>
@stop