
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Register &mdash; Sipadu</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('admin/assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/css/components.css') }}">

</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
            <div class="login-brand">
              <img src="{{ asset('logo.png') }}" alt="logo" width="300">
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Register</h4></div>

              <div class="card-body">
                <form method="POST" action="/postregister" class="needs-validation" novalidate="" oninput='password2.setCustomValidity(password2.value != password.value ? "Passwords tidak sama." : "")'>
                  {{csrf_field()}}
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="frist_name">Nama Lengkap</label>
                      <input id="frist_name" type="text" class="form-control {{$errors->has('nama') ? 'has-error' : ''}}" name="nama" autofocus placeholder="Nama Lengkap" value="{{old('nama')}}" required>
                      @if($errors->has('nama'))
                        <span class="help-block">{{$errors->first('nama')}}</span>
                      @endif
                    </div>
                    <div class="form-group col-6">
                      <label for="last_name">NIP/NIK</label>
                      <input id="last_name" type="text" class="form-control {{$errors->has('nip') ? 'has-error' : ''}}" name="nip" placeholder="NIK / NIP (Input salah satu)" value="{{old('nip')}}" required>
                      @if($errors->has('nip'))
                        <span class="help-block">{{$errors->first('nip')}}</span>
                      @endif
                    </div>
                  </div>                  
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control {{$errors->has('email') ? 'has-error' : ''}}" name="email" value="{{old('email')}}" required>
                    @if($errors->has('email'))
                        <span class="help-block">{{$errors->first('email')}}</span>
                    @endif
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label for="password" class="d-block">Password</label>
                      <input id="password" type="password" class="form-control pwstrength {{$errors->has('password') ? 'has-error' : ''}}" data-indicator="pwindicator" name="password">
                      <div id="pwindicator" class="pwindicator">
                        <div class="bar"></div>
                        <div class="label"></div>
                        @if($errors->has('password'))
                        <span class="help-block">{{$errors->first('password')}}</span>
                    @endif
                      </div>
                    </div>
                    <div class="form-group col-6">
                      <label for="password2" class="d-block">Konfirmasi Password</label>
                      <input id="password2" type="password" class="form-control" name="password2">
                      <div class="invalid-feedback">
                      Konfirmasi Password Anda
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                      Register
                    </button>
                  </div>
                  <div class="mt-5 text-muted text-center">
                    Masuk Akun? <a href="/login">Login</a>
                  </div>
                </form>
              </div>
            </div>
            <div class="simple-footer">
              Copyright &copy; UTIPD 2020
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <!-- General JS Scripts -->
  <script src=" {{ asset('admin/assets/js/modul/jquery.min.js') }}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('admin/assets/js/modul/jquery.pwstrength.min.js') }} "></script>
  

  <!-- Page Specific JS File -->
  <script src="{{ asset('admin/assets/js/modul/auth-register.js') }} "></script>
</body>
</html>