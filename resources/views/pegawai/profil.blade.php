@extends('layouts.master')

@section('title')
Profil {{$pegawai->nama}}
@endsection

@section('scripts')
  
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!-- Datepicker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css"/>

<!-- Daterangepicker -->
<script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left'
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
  });
});
</script>
<!-- Datepicker -->
<script>
    $(document).ready(function(){
      var date_input=$('input[name="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>
<script>
    $(document).ready(function(){
      var date_input=$('input[name="date1"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Data Lengkap {{$pegawai->nama}}</h1>
  </div>
  <div class="section-body">
    <div class="row">
                  <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                              <div class="col-lg-4">
                                <div class="border-bottom text-center pb-4">
                                  <img src=" {{ $pegawai->getAvatar()}} " width="100" height="100" alt="profile" class="img-lg rounded-circle mb-3">
                                  
                                </div>
                                <div class="py-4">
                                  <h5>Detail Data Diri</h5>
                                  <p class="clearfix">
                                    <span class="float-left"> Satuan </span>
                                    <span class="float-right text-muted"> {{$pegawai->atasan->satuan}} </span>
                                  </p>
                                  <p class="clearfix">
                                    <span class="float-left"> Jabatan </span>
                                    <span class="float-right text-muted"> {{$pegawai->jabatan->jabatan}}</span>
                                  </p>
                                  <p class="clearfix">
                                    <span class="float-left"> Email </span>
                                    <span class="float-right text-muted"> {{$pegawai->email}}</span>
                                  </p>
                                  <p class="clearfix">
                                    <span class="float-left"> Alamat </span>
                                    <span class="float-right text-muted"> {{$pegawai->alamat}} </span>
                                  </p>
                                </div>
                                <div class="d-flex justify-content-between">
                                <a href="/pegawai/{{$pegawai->id}}/edit"><button class="btn btn-primary btn-block">Edit Profil</button></a>
                                <a href="#"><button onclick="window.history.back();" class="btn btn-primary btn-block">Kembali</button></a>
                                </div>
                              </div>
                              <div class="col-lg-8">
                                <div class="d-flex justify-content-between">
                                  <div>
                                    <h3>{{$pegawai->nama}}</h3>
                                  </div>
                                    <div class="card-header-action"><a href="#" data-toggle="modal" data-target="#modalcuti" class="btn btn-primary">Tambah Cuti <i class="fas fa-plus"></i></a>
                                    </div>
                                </div>
                                <h5 class="card-title">History Cuti</h5>
                                <div class="table-responsive border rounded p-1">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th><strong>Jenis Cuti</strong></th>
                                      <th><strong>Mulai</strong></th>
                                      <th><strong>Sampai</strong></th>
                                      <th><strong>Status</strong></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @forelse($pegawai->cuti as $cuti)
                                    <tr>
                                      <td><a href="/laporan/{{ $cuti->id}}/status">{{ $cuti->jenis}}</a></td>
                                      <td>{{$cuti->dari}}</td>
                                      <td>{{$cuti->sampai}}</td>

                                      @if ($cuti->status == "Diterima")
                                        <td><label class="badge badge-success">{{$cuti->status}}</label></td>
                                      @elseif($cuti->status == "Ditolak")
                                        <td><label class="badge badge-danger">{{$cuti->status}}</label></td>
                                      @else
                                        <td><label class="badge badge-warning">{{$cuti->status}}</label></td>
                                      @endif
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="4"><center><b>Tidak ada Data Cuti</b></center></td>
                                    </tr>
                                    @endforelse
                                  </tbody>
                                </table>
                                </div>
                      </div>
                    </div>
                  </div>
                </div>
  </div>
</section>
<!-- Modal -->
        <div class="modal fade" id="modalcuti" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Cuti</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="/pegawai/{{$pegawai->id}}/addcuti" method="POST">
                  {{csrf_field()}}
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Jenis Cuti</label>
                    <select name="jenis" class="form-control" id="exampleFormControlSelect1">
                      <option value="Tahunan">Tahunan</option>
                      <option value="Umum">Umum</option>
                    </select>
                  </div>
                  <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="inputEmail4">Mulai</label>
                            <input type="text" class="form-control datepicker" id="date1" name="date1">
                          </div>
                          <div class="form-group col-md-6">
                            <label for="inputPassword4">Sampai</label>
                            <input type="text" class="form-control datepicker" id="date" name="date">
                          </div>
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
              </div>
            </div>     
          </div>
  </div>
</div>
@stop