@extends('layouts.master')

@section('title')
Profil {{$pegawai->nama}}
@endsection

@section('scripts')

<!-- Datepicker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css"/>

<!-- Datepicker -->
<script>
    $(document).ready(function(){
      var date_input=$('input[name="tmt"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Data Lengkap {{$pegawai->nama}}</h1>
  </div>
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
      {{session('sukses')}}
    </div>
    @endif
  <div class="section-body">
            <h2 class="section-title">Hi, {{$pegawai->nama}}!</h2>
            <p class="section-lead">
              Ubah informasi mengenai diri anda di halaman ini.
            </p>
            <div class="row mt-sm-4">
              <div class="col-12 col-md-12 col-lg-5">
                <div class="card profile-widget">
                  <div class="profile-widget-header">                     
                    <img alt="image" src="{{ $pegawai->getAvatar()}}" class="rounded-circle profile-widget-picture" data-toggle="tooltip" title="" data-original-title="{{$pegawai->nama}}">
                  </div>
                  <div class="profile-widget-description">
                    <div class="profile-widget-name">{{$pegawai->nama}} <div class="text-muted d-inline font-weight-normal"><div class="slash"></div> {{$pegawai->jabatan->jabatan}} - {{$pegawai->atasan->satuan}}</div></div>
                    {{$pegawai->nama}} adalah seorang  <b>{{$pegawai->jabatan->jabatan}}</b> di <b>{{$pegawai->satuan}}</b> IAIN Ponorogo, memulai karir di kampus ini sejak <b>
                      @if($pegawai->tmt == null)
                      Belum Mengisi TMT
                      @else
                      {{$pegawai->tmt->format('d M Y')}}
                      @endif
                      </b>. Dan telah mengabdi di IAIN Ponorogo selama <b>
                      @if ($pegawai->tmt == null)
                        Belum mengisi TMT
                      @else
                        {{$pegawai->tmt->diffForHumans()}}
                      @endif
                      </b>. Saat ini anda tinggal di <b>{{$pegawai->alamat}}</b>.
                  </div> 
                </div>
              </div>
              <div class="col-12 col-md-12 col-lg-7">
                <div class="card">
                  <form action="/update" method="POST" class="needs-validation" novalidate="" enctype="multipart/form-data" oninput='password2.setCustomValidity(password2.value != password.value ? "Passwords tidak sama." : "")'>
                        {{csrf_field()}} 
                    <div class="card-header">
                      <h4>Edit Profil</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">                               
                          <div class="form-group col-md-6 col-12">
                            <label>Nama (lengkap beserta gelar)</label>
                            <input type="text" name="nama" class="form-control {{$errors->has('nama') ? 'has-error' : ''}}" value="{{$pegawai->nama}}" required="">
                            <div class="invalid-feedback">
                              Mohon isi nama lengkap
                            </div>
                            @if($errors->has('nama'))
                            <span class="help-block">{{$errors->first('nama')}}</span>
                            @endif
                          </div>
                          <div class="form-group col-md-6 col-12">
                            <label>NIP/NITK</label>
                            <input type="text" name="nip"class="form-control {{$errors->has('nip') ? 'has-error' : ''}}" value="{{$pegawai->nip}}" required="">
                            <div class="invalid-feedback">
                              Mohon isi NIP atau NITK anda
                            </div>
                            @if($errors->has('nip'))
                            <span class="help-block">{{$errors->first('nip')}}</span>
                            @endif
                          </div>
                        </div>
                        <div class="row">                               
                          <div class="form-group col-md-6 col-12">
                            <label>Satuan</label>
                            <select name="atasan" class="form-control">
                              @foreach($atasan as $atas)
                                <option value="{{$atas->id}}" 
                                @if($atas->id == $pegawai->atasan_id)
                                    selected=""
                                @endif
                                >{{$atas->satuan}}</option>
                              @endforeach
                            </select>
                            {{-- <input type="text" name="satuan" class="form-control" value="{{$pegawai->satuan}}" required=""> --}}
                            <div class="invalid-feedback">
                              Mohon isi satuan kerja anda
                            </div>
                          </div>
                          <div class="form-group col-md-6 col-12">
                            <label>Jabatan</label>
                            <select name="jabatan" class="form-control">
                              @foreach($jabatan as $jabat)
                                <option value="{{$jabat->id}}" 
                                @if($jabat->id == $pegawai->jabatan_id)
                                    selected
                                @endif
                                >{{$jabat->jabatan}}</option>
                              @endforeach
                            </select>

                            {{-- <input type="text" name="jabatan" class="form-control" value="{{$pegawai->jabatan}}" required=""> --}}
                            <div class="invalid-feedback">
                              Mohon isi jabatan
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12 col-12">
                            <label>Alamat</label>
                            <textarea name="alamat" class="form-control {{$errors->has('alamat') ? 'has-error' : ''}}" required="">{{$pegawai->alamat}}</textarea>
                            {{-- <input type="email" name="alamat" class="form-control" value="{{$pegawai->alamat}}" required=""> --}}
                            <div class="invalid-feedback">
                              Mohon isi email
                            </div>
                            @if($errors->has('alamat'))
                            <span class="help-block">{{$errors->first('alamat')}}</span>
                            @endif
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-7 col-12">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control {{$errors->has('email') ? 'has-error' : ''}}" value="{{$pegawai->email}}" required=""  >
                            <div class="invalid-feedback">
                              Mohon isi email
                            </div>
                            @if($errors->has('email'))
                            <span class="help-block">{{$errors->first('email')}}</span>
                            @endif
                          </div>
                          <div class="form-group col-md-5 col-12">
                            <label>Tertanggal Masuk (TMT)</label>
                            <input type="text" name="tmt" class="form-control" value="{{$pegawai->tmt}}" placeholder="Format TMT">
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6 col-12">
                            <label>Foto Profil</label>
                            <input type="file" name="avatar" class="form-control {{$errors->has('avatar') ? 'has-error' : ''}}" placeholder="Format TMT">
                            @if($errors->has('avatar'))
                            <span class="help-block">{{$errors->first('avatar')}}</span>
                            @endif
                            <div class="form-text text-muted">Foto Profil maks. 200 Kb jpg/png</div>
                          </div>
                          <div class="form-group col-md-6 col-12">
                            <label>Nomor Telefon</label>
                            <input type="number" name="telp" class="form-control {{$errors->has('telp') ? 'has-error' : ''}}" value="{{$pegawai->telp}}" placeholder="Isi Notelp Anda" maxlength="12">
                            @if($errors->has('telp'))
                            <span class="help-block">{{$errors->first('telp')}}</span>
                            @endif
                          </div>
                        </div>
                        <div class="row">                               
                          <div class="form-group col-md-6 col-12">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" required="" >
                            <div class="invalid-feedback">
                              Mohon isikan password
                            </div>
                          </div>
                          <div class="form-group col-md-6 col-12">
                            <label>Konfirmasi Password</label>
                            <input type="password" name="password2" class="form-control" required="" >
                            <div class="invalid-feedback">
                              Password tidak sama
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
</section>
@stop