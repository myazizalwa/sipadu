@extends('layouts.master')

@section('title')
Edit Data Pegawai
@endsection

@section('content')

  <section class="section">
    <div class="section-header">
      <h1>Edit Data Pegawai</h1>
    </div>
    <div class="section-body">
      <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                    <form class="forms-sample" action="/pegawai/{{$pegawai->id}}/update" method="POST" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label"><strong>Absen Online</strong></label>
                        <label class="custom-switch mt-2">
                          <input type="checkbox" name="status" class="custom-switch-input" @if ($pegawai->user->status == 1)
                              checked
                          @endif>
                          <span class="custom-switch-indicator"></span>
                          <span class="custom-switch-description">Aktifkan / Non Aktifkan Absensi</span>
                        </label>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Nama Lengkap" value="{{$pegawai->nama}}" name="nama">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputEmail2" class="col-sm-3 col-form-label">NIP</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputEmail2" placeholder="NIP" value="{{$pegawai->nip}}" name="nip">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">Satuan</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputMobile" placeholder="Satuan" value="{{$pegawai->atasan->satuan}}" name="satuan" disabled>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Jabatan</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputMobile" placeholder="Satuan" value="{{$pegawai->jabatan->jabatan}}" name="satuan" disabled >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputConfirmPassword2" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                          <textarea class="form-control" id="exampleTextarea1" rows="4" name="alamat">{{$pegawai->alamat}}</textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputConfirmPassword2" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="exampleInputConfirmPassword2" placeholder="Email" value="{{$pegawai->email}}" name="email">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputConfirmPassword2" class="col-sm-3 col-form-label">Foto</label>
                        <div class="col-sm-9">
                          <input type="file" class="form-control" id="exampleInputConfirmPassword2" name="avatar">
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary mr-2">Update</button>
                      <button onclick="goBack()" class="btn btn-light" >Cancel</button>
                    </form>
                  </div>
                </div>
              </div>
                  <script>
          function goBack() {
            window.history.back();
          }
           </script>
      </div>
    </div>
  </section> 
            
@stop