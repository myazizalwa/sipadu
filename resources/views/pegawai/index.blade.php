@extends('layouts.master')

@section('title')
Data Pegawai
@endsection

@section('css')
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
@endsection
@section('scripts')

<!-- JS Libraies -->
<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/jquery-ui/jquery-ui.min.js"></script>

<!-- Page Specific JS File -->
<script src="{{ asset('admin/assets/js/modul/modul-datatable.js') }}"></script>

@endsection


@section('content')
<section class="section">
  <div class="section-header">
    <h1>List Data Pegawai</h1>
  </div>
    @if(session('sukses'))
        <div class="alert alert-success" role="alert">
          {{session('sukses')}}
        </div>
    @endif    
  <div class="section-body">
      
	  <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>                                 
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody> 
                            <!--@php $no = 0; @endphp-->
                            @php $no = 0; @endphp
                            @foreach ($data_pegawai as $pegawai)
                            @php $no++; @endphp
							<tr>
							    <td>{{$no}}</td>
								<td><a href="/pegawai/{{$pegawai->id}}/profil"><p>{{ $pegawai->nama}}</p></a></td>
								<td>{{ $pegawai->email}}</td>
								<td>
									<a href="/pegawai/{{$pegawai->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
									<a href="/pegawai/{{$pegawai->id}}/reset" class="btn btn-success btn-sm" onclick="return confirm('Password akan direset menjadi 123 ?')">Reset</a>
									<a href="/pegawai/{{$pegawai->id}}/hapus" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus, beserta data absen dan cuti?')">Hapus</a>
									<a href="/laporan/absen/{{$pegawai->id}}" class="btn btn-icon btn-primary btn-sm">Presensi</a>
								</td>
							</tr>
						    @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
  </div>
</section>
<!-- Modal -->
                  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Tambah Pegawai</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <form action="/pegawai/tambah" method="POST">
				        	{{csrf_field()}}
						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama Lengkap</label>
						    <input name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">NIP</label>
						    <input name="nip" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="NIP/ No. Pegawai">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Satuan</label>
						    <input name="satuan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Bagian/Unit/Fakultas">
						  </div>
						  <div class="form-group">
						    <label for="exampleFormControlSelect1">Jabatan</label>
						    <select name="jabatan" class="form-control" id="exampleFormControlSelect1">
						      <option value="Kepala">Kepala</option>
						      <option value="Staff">Staff</option>
						    </select>
						  </div>
						  <div class="form-group">
						    <label for="exampleFormControlTextarea1">Alamat</label>
						    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Email</label>
						    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
						  </div>
						  <div class="modal-footer">
				        <button type="submit" class="btn btn-primary">Simpan</button>
				      	</form>
				      </div>
				      </div>
					</div>
	</div>
</div>
@stop