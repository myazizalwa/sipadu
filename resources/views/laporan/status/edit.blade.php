@extends('layouts.master')

@section('title')
Approval Status Cuti
@endsection

@section('content')

<section class="section">
    <div class="section-header">
      <h1>Approval Status Cuti   <strong>{{ $cuti->pegawai->nama}}</strong></h1>
    </div>
    <div class="section-body">
      <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                  <div class="card-body">
                      <form class="forms-sample" action="/laporan/{{$cuti->id}}/update" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}  
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="inputEmail4">Jenis Cuti</label>
                            <input type="text" class="form-control" name="jenis" value="{{$cuti->jenis}}" readonly="">
                          </div>
                          <div class="form-group col-md-6">
                            <label for="inputPassword4">Alasan</label>
                            <input type="text" class="form-control" name="alasan" value="{{$cuti->alasan}}" readonly="">
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="inputCity">Mulai</label>
                            <input type="text" class="form-control" name="dari" value="{{$cuti->dari}}" readonly="">
                          </div>
                          <div class="form-group col-md-4">
                            <label for="inputState">Sampai</label>
                            <input type="text" class="form-control" name="sampai" value="{{$cuti->sampai}}" readonly="">
                          </div>
                          <div class="form-group col-md-2">
                            <label for="inputZip">Lama Cuti (Hari)</label>
                            <input type="text" class="form-control" name="jumlah" value="{{$cuti->jumlah}}" readonly="">
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="inputEmail4">Alamat saat Cuti</label>
                            <input type="text" class="form-control" name="alamat" value="{{$cuti->alamat}}" readonly="">
                          </div>
                          <div class="form-group col-md-6">
                            <label for="inputPassword4">Nomor Telp saat Cuti</label>
                            <input type="text" class="form-control" name="notelp" value="{{$cuti->notelp}}" readonly="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputAddress2">Status</label>
                          <select name="status" class="form-control">
                                <option value="Usulan" @if( $cuti->status == 'Usulan') selected @endif>Usulan</option>
                                <option value="Diterima" @if( $cuti->status == 'Diterima') selected @endif>Diterima</option>
                                <option value="Ditolak" @if( $cuti->status == 'Ditolak') selected @endif>Ditolak</option>
                                <option value="Pending" @if( $cuti->status == 'Pending') selected @endif>Pending</option>
                                </select>
                        </div>
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="inputEmail4">Keterangan</label>
                            <input type="text" name="keterangan" value="{{$cuti->keterangan}}" class="form-control {{$errors->has('keterangan') ? 'has-error' : ''}}" placeholder="Isi Alasan Diterima / - (apabila kosong)">
                            @if($errors->has('keterangan'))
                                  <span class="help-block">{{$errors->first('keterangan')}}</span>
                            @endif
                          </div>
                          <div class="form-group col-md-6">
                            <label for="inputPassword4">File Penerimaan Cuti (pdf / maks 500kb)</label>
                            <input type="file" class="form-control {{$errors->has('penerimaan') ? 'has-error' : ''}}" name="penerimaan">
                            @if($errors->has('penerimaan'))
                                  <span class="help-block">{{$errors->first('penerimaan')}}</span>
                            @endif
                          </div>
                        </div>
                        <div class="card-footer">
                          <a target="_blank" href="{{ $cuti->getPengajuan()}}"><button type="button" class="btn btn-success">Lihat Dokumen</button></a>
                        </div>
                        <div class="card-footer">
                          <a href="/laporan/approve" class="btn btn-primary">Kembali</a>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                  </div>
                  </form>                  
                </div>

                  <script>
          function goBack() {
            window.history.back();
          }
           </script>
      </div>
    </div>
  </section> 

<!-- Modal -->
<div class="modal fade" id="viewlampiran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">File Lampiran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class='embed-responsive' style='padding-bottom:150%'>
            <object data='http://eprints.uny.ac.id/14516/7/9.%20DAFTAR%20LAMPIRAN.pdf' type='application/pdf' width='100%' height='100%'></object>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection