<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <title>Lampiran Cuti - {{$cetakcuti->pegawai->nama}}</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        .text-right {
            text-align: right;
        }
    </style>
</head>
<body class="login-page" style="background: white">

    <div>
        <div class="row">
            <div class="col-xs-7">
                &nbsp;
            </div>

            <div class="col-xs-4">
                ANAK LAMPIRAN 1.b<br>
				PERATURAN BADAN KEPEGAWAIAN NEGARA<br>
				REPUBLIK INDONESIA<br>
				NOMOR 24 TAHUN 2017<br>
				TENTANG<br>
				TATA CARA PEMBERIAN CUTI PEGAWAI NEGERI SIPIL<br>


                <br>
                Ponorogo, {{$sekarang->format('d M Y')}}<br>
				Kepada<br>
				Yth. Rektor IAIN Ponorogo<br>
				Di     Ponorogo<br>

				<br>
            </div>
        </div>

        <p align="center">FORMULIR  PERMINTAAN DAN PEMBERIAN CUTI</p>
        <div style="margin-bottom: 0px">&nbsp;</div>

        <div class="row">
        	<div class="col-md-12">
        		<table class="table table-bordered">
				  <thead>
				    <tr>
				      <th colspan="4">I. DATA PEGAWAI</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td width="10%">Nama</td>
				      <td width="40%">{{$cetakcuti->pegawai->nama}}</td>
				      <td width="10%">NIP/NITK</td>
				      <td>{{$cetakcuti->pegawai->nip}}</td>
				    </tr>
				    <tr>
				      <td scope="row">Jabatan</td>
				      <td>{{$cetakcuti->pegawai->jabatan->jabatan}}</td>
				      <td>Masa Kerja</td>
				      <td>{{$cetakcuti->pegawai->tmt->diffForHumans()}}</td>
				    </tr>
				    <tr>
				      <td>Unit Kerja</td>
				      <td colspan="3">{{$cetakcuti->pegawai->atasan->satuan}} - IAIN Ponorogo</td>
				    </tr>
				  </tbody>
				</table>
        	</div>
        </div>

        <div class="row">
        	<div class="col-md-12">
        		<table class="table table-bordered">
				  <thead>
				    <tr>
				      <th colspan="4">II. JENIS CUTI YANG DIAMBIL**</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td>1. Cuti Tahunan</td>
				      <td width="10%">
						  	@if ($cetakcuti->jenis == "Tahunan")                                
                                <input type="checkbox" checked disabled>
                            @endif
				      </td>
				      <td>2. Cuti Besar</td>
				      <td width="10%"></td>
				    </tr>
				    <tr>
				      <td>3. Cuti Sakit</td>
				      <td></td>
				      <td>4. Cuti Melahirkan</td>
				      <td></td>
				    </tr>
				    <tr>
				      <td>5. Cuti Karena Alasan Penting</td>
				      <td></td>
				      <td>6. Cuti di Luar Tanggunan Negara</td>
				      <td></td>
				    </tr>
				  </tbody>
				</table>
        	</div>
        </div>

        <div class="row">
        	<div class="col-md-12">
        		<table class="table table-bordered">
				  <thead>
				    <tr>
				      <th colspan="4">III. ALASAN CUTI</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td colspan="4">{{$cetakcuti->alasan}}</td>
				    </tr>
				  </tbody>
				</table>
        	</div>
        </div>

        <div class="row">
        	<div class="col-md-12">
        		<table class="table table-bordered">
				  <thead>
				    <tr>
				      <th colspan="6">IV. LAMANYA CUTI</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td width="10%">Selama</td>
				      <td width="40%">{{$cetakcuti->jumlah}} hari</td>
				      <td width="10%">mulai tanggal</td>
				      <td>{{$cetakcuti->dari}}</td>
				      <td width="10%">sampai</td>
				      <td>{{$cetakcuti->sampai}}</td>
				    </tr>
				  </tbody>
				</table>
        	</div>
        </div>

        <div class="row">
        	<div class="col-md-12">
        		<table class="table table-bordered">
				  <thead>
				    <tr>
				      <th colspan="5">V. CATATAN CUTI***</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td colspan="3">1. CUTI TAHUNAN</td>
				      <td>2. CUTI BESAR</td>
				      <td></td>
				    </tr>
				    <tr>
				      <td>Tahun</td>
				      <td>Sisa</td>
				      <td>Keterangan</td>
				      <td>3. CUTI SAKIT</td>
				      <td></td>
				    </tr>
				    <tr>
				      <td>N-2</td>
				      <td></td>
				      <td></td>
				      <td>4. CUTI MELAHIRKAN</td>
				      <td></td>
				    </tr>
				    <tr>
				      <td>N-1</td>
				      <td></td>
				      <td></td>
				      <td>5. CUTI KARENA ALASAN PENTING</td>
				      <td></td>
				    </tr>
				    <tr>
				      <td>N</td>
				      <td></td>
				      <td></td>
				      <td>6. CUTI DI LUAR TANGGUNGAN NEGARA</td>
				      <td></td>
				    </tr>
				  </tbody>
				</table>
        	</div>
        </div>

        <div class="row">
        	<div class="col-md-12">
        		<table class="table table-bordered">
				  <thead>
				    <tr>
				      <th colspan="3">VI. ALAMAT SELAMA MENJALANKAN CUTI</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td width="60%">&nbsp;</td>
				      <td width="10%">TELP</td>
				      <td>{{$cetakcuti->notelp}}</td>
				    </tr>
				    <tr>
				      <td>{{$cetakcuti->alamat}}</td>
				      <td colspan="2">
				      	Hormat Saya,<br>
				      	<br><br>

				      <u>{{$cetakcuti->pegawai->nama}}</u><br>
				      NIP. {{$cetakcuti->pegawai->nip}}
				      </td>
				    </tr>
				  </tbody>
				</table>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<table class="table table-bordered">
				  <thead>
				    <tr>
				      <th colspan="4">VII. PERTIMBANGAN ATASAN LANGSUNG ***</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td>DISETUJUI</td>
				      <td>PERUBAHAN ****</td>
				      <td>DITANGGUHKAN ****</td>
				      <td>TIDAK DISETUJUI ****</td>
				    </tr>
				    <tr>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				    </tr>
				    <tr>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				      <td>
				      	<br>
				      	<br><br>

				      	<u>{{$cetakcuti->pegawai->atasan->kepala}}</u><br>
				     	NIP. {{$cetakcuti->pegawai->atasan->nip}}
				      </td>
				    </tr>
				  </tbody>
				</table>
        	</div>
        </div>

        <div class="row">
        	<div class="col-md-12">
        		<table class="table table-bordered">
				  <thead>
				    <tr>
				      <th colspan="4">VIII. KEPUTUSAN PEJABAT YANG BERWENANG MEMBERIKAN CUTI **</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td>DISETUJUI</td>
				      <td>PERUBAHAN ****</td>
				      <td>DITANGGUHKAN ****</td>
				      <td>TIDAK DISETUJUI ****</td>
				    </tr>
				    <tr>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				    </tr>
				    <tr>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
				      <td>
				      	<br>
				      	<br><br>

				      	<u>Dr. Hj. S. Maryam Yusuf, M.Ag</u><br>
				     	NIP. 195705061983032002
				      </td>
				    </tr>
				  </tbody>
				</table>
        	</div>
        </div>

        <div style="margin-bottom: 0px">&nbsp;</div>

            <div class="row">
            	<div class="col-xs-8">
            		<p>
            		Catatan<br>
					*	Coret yang tidak perlu<br>
					**	Pilih salah satu dengan memberi tanda centang <input type="checkbox" checked disabled></span><br>
					***	Diisi oleh pejabat yang menangani bidang kepegawaian sebelum PNS mengajukan cuti<br>
					****	Diberi tanda centang dan alasannya<br>
					N	Cuti tahun berjalan<br></p>
            	</div>
            </div>
        </div>
    </body>
    </html>