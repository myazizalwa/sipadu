@extends('layouts.master')

@section('title')
Cuti Yang Ditolak
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Cuti Yang Ditolak</h1>
  </div>
  <div class="section-body">
      <div class="row">
      	<div class="col-md-12">
      		<div class="card">
		      <div class="card-body p-0"><div class="table-responsive table-invoice">
		      	<table class="table table-striped">
		      		<tbody>
		      			<tr>
		      				<th><h6>Nama</h6></th>
							<th><h6>Jenis Cuti</h6></th>
							<th><h6>Alasan</h6></th>
							<th><h6>Periode Cuti</h6></th>
							<th><h6>Status</h6></th>
		      			</tr> 
		      			@foreach ($data_reject as $cuti)
							<tr>
								<td><a href="/laporan/{{ $cuti->id}}/status">{{ $cuti->pegawai->nama}}</a></td>
								<td>{{ $cuti->jenis}}</td>
								<td>{{ $cuti->alasan}}</td>
								<td><strong>{{ $cuti->dari}}</strong> sampai <strong>{{ $cuti->sampai}}</strong></td>

								@if ($cuti->status == "Diterima")
                                        <td><label class="badge badge-success">{{$cuti->status}}</label></td>
                                      @elseif($cuti->status == "Ditolak")
                                        <td><label class="badge badge-danger">{{$cuti->status}}</label></td>
                                      @else
                                        <td><label class="badge badge-warning">{{$cuti->status}}</label></td>
                                @endif

							</tr>
						@endforeach
		      		</tbody>
		      	</table> <!---->
		      </div>
		  </div>
		</div>
	</div>
	</div>
  </div>
</section>
@stop