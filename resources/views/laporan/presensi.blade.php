@extends('layouts.master')

@section('title')
Data Presensi Seluruh Pegawai
@endsection

@section('css')
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
@endsection
@section('scripts')

<!-- JS Libraies -->
<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/jquery-ui/jquery-ui.min.js"></script>

<!-- Page Specific JS File -->
<script src="{{ asset('admin/assets/js/modul/modul-datatable.js') }}"></script>
{{-- <script src="https://demo.getstisla.com/assets/js/page/modules-datatables.js"></script> --}}

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
  //KETIKA PERTAMA KALI DI-LOAD MAKA TANGGAL NYA DI-SET TANGGAL SAA PERTAMA DAN TERAKHIR DARI BULAN SAAT INI
  $(document).ready(function() {
      let start = moment().startOf('week')
      let end = moment().endOf('week')
      
      //INISIASI DATERANGEPICKER
      $('#date').daterangepicker({
          startDate: start,
          endDate: end
      })
  })
</script>

@endsection


@section('content')
<section class="section">
  <div class="section-header">
    <h1>List Presensi Pegawai</h1>
  </div>
    @if(session('sukses'))
        <div class="alert alert-success" role="alert">
          {{session('sukses')}}
        </div>
    @endif    
  <div class="section-body">
      
	  <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <form action="/laporan/presensi" method="get">
                      <div class="input-group mb-3 col-md-4 float-right">
                          <input type="text" id="date" name="date" class="form-control">
                          <div class="input-group-append">
                              <button class="btn btn-primary" type="submit">Filter</button>
                          </div>
                      </div>
                  </form>
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>                                 
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Hari/Tgl</th>
                            <th>Nama</th>
                            <th>NIP</th>
                            <th>Jabatan</th>
                            <th>Jam Masuk</th>
                            <th>Jam Pulang</th>
                          </tr>
                        </thead>
                        <tbody> 
                            <!--@php $no = 0; @endphp-->
                            @php $no = 0; @endphp
                            @foreach ($presensi as $absen)
                            @php $no++; @endphp
            							<tr>
            							    <td>{{$no}}</td>
                              <td>{{$absen->user->tglIndo($absen->date)}}</td>
                              <td>{{$absen->user->pegawai->nama}}</td>
                              <td>{{$absen->user->pegawai->nip}}</td>
                              <td>{{$absen->user->pegawai->jabatan->jabatan}}</td>
                              @if($absen->time_masuk >= '08.00')
                                  <td><div class="badge badge-danger">{{ date('H:i', strtotime($absen->time_masuk)) }}</div></td>
                              @else
                                   <td>{{ date('H:i', strtotime($absen->time_masuk)) }}</td>   
                              @endif
                              @if(is_null($absen->time_pulang))
                                  <td></td>
                              @elseif($absen->time_pulang <= '15.00')
                                   <td><div class="badge badge-warning">{{ date('H:i', strtotime($absen->time_pulang)) }}</div></td>
                              @else
                                  <td>{{ date('H:i', strtotime($absen->time_pulang)) }}</td>
                              @endif
                              {{-- <td>{{ date('H:i', strtotime($absen->time_masuk)) }}</td>
                              <td>{{ date('H:i', strtotime($absen->time_pulang)) }}</td> --}}
            							</tr>
						                @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
  </div>
</section>
@endsection