<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Absen {{$user->name}}</title>
    <link rel="stylesheet" href="<a class="vglnk" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css" rel="nofollow">

    <style>
    @page { size: A4 }
 
    h1 {
        font-weight: bold;
        font-size: 20pt;
        text-align: center;
    }
    
    h2 {
        font-weight: bold;
        font-size: 13pt;
        text-align: center;
    }
    
    h3 {
        font-weight: bold;
        font-size: 10pt;
        text-align: center;
    }
 
    table {
        border-collapse: collapse;
        width: 100%;
    }
 
    .table th {
        padding: 8px 8px;
        border:1px solid #000000;
        text-align: center;
    }
 
    .table td {
        padding: 3px 3px;
        border:1px solid #000000;
    }
 
    .text-center {
        text-align: center;
    }

    .badge {
        vertical-align: middle;
        padding: 7px 7px;
        font-weight: 600;
        letter-spacing: .3px;
        border-radius: 15px;
        font-size: 13px;
    }

    .badge-danger {
        color: #fff;
        background-color: #dc3545;
    }

    .badge-warning {
        color: #212529;
        background-color: #ffc107;
    }

</style>
</head>
<body class="A4">
    <section class="sheet padding-10mm">
        <table>
            <tr align="center">
                <td><img src="{{ asset('head.png') }}"></td>
                <td colspan="4">
                    <h2>LAPORAN PRESENSI & KINERJA</h2>
                    <h3>(SIPADU - Presensi Online Tanggap COVID-19)</h3>
                </td>
                <td><img src="{{ asset('sipadu.png') }}"></td>
            </tr>
          <tr>
            <td width="50">Nama</td>
            <td width="10">:</td>
            <td><strong>{{$user->name}}</strong></td>
          </tr>
          <tr>
            <td width="50">NIP / NITK</td>
            <td width="10">:</td>
            <td>{{$user->pegawai->nip}}</td>
          </tr>
          <tr>
            <td width="50">Jabatan</td>
            <td width="10">:</td>
            <td>{{$user->pegawai->jabatan->jabatan}}</td>
          </tr>
          <tr>
            <td width="50">Periode</td>
            <td width="10">:</td>
            <td>{{ $user->tglIndo($date[0]) }} s/d {{ $user->tglIndo($date[1]) }}</td>
          </tr>
        </table><br>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tanggal</th>
                    <th>Datang</th>
                    <th>Pulang</th>
                    <th>LKH</th>
                </tr>
            </thead>
            <tbody>
                @php $no = 0; @endphp
                @forelse ($presensi as $abs)
                @php $no++; @endphp
                    <tr>
                        <td class="text-center" width="20">{{$no}}</td>
                        <td width="110">{{$user->tglIndo($abs->date)}}</td>
                        @if($abs->time_masuk >= '07:31')
                            <td class="text-center" width="60"><div class="badge badge-danger">{{ date('H:i', strtotime($abs->time_masuk)) }}</div></td>
                        @else
                             <td class="text-center" width="60">{{ date('H:i', strtotime($abs->time_masuk)) }}</td>   
                        @endif
                        
                        @if(is_null($abs->time_pulang))
                            <td></td>
                        @elseif($abs->time_pulang <= '16:00')
                             <td class="text-center" width="60"><div class="badge badge-warning">{{ date('H:i', strtotime($abs->time_pulang)) }}</div></td>
                        @else
                            <td class="text-center" width="60">{{ date('H:i', strtotime($abs->time_pulang)) }}</td>
                        @endif
                        <td>{{ $abs->lkh}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4"><center><b>Tidak ada Data Presensi</b></center></td>
                    </tr>
                @endforelse
            </tbody>
        </table><br>
        <p style="text-align:right">Mengetahui,<br>
            Kepala {{$user->pegawai->atasan->satuan}}
            <br><br><br><br>

        <u>{{$user->pegawai->atasan->kepala}}</u><br>
        NIP. {{$user->pegawai->atasan->nip}}</p>
    </section>
</body>
</html>