@extends('layouts.master')

@section('title')
Laporan Absen {{$pegawai->nama}}
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    //KETIKA PERTAMA KALI DI-LOAD MAKA TANGGAL NYA DI-SET TANGGAL SAA PERTAMA DAN TERAKHIR DARI BULAN SAAT INI
    $(document).ready(function() {
        let start = moment().startOf('month')
        let end = moment().endOf('month')

        //KEMUDIAN TOMBOL EXPORT PDF DI-SET URLNYA BERDASARKAN TGL TERSEBUT
        $('#exportpdf').attr('href', '/laporan/absen/pdf/{{$pegawai->id}}/' + start.format('YYYY-MM-DD') + '+' + end.format('YYYY-MM-DD'))

        //INISIASI DATERANGEPICKER
        $('#date').daterangepicker({
            startDate: start,
            endDate: end
        }, function(first, last) {
            //JIKA USER MENGUBAH VALUE, MANIPULASI LINK DARI EXPORT PDF
            $('#exportpdf').attr('href', '/laporan/absen/pdf/{{$pegawai->id}}/' + first.format('YYYY-MM-DD') + '+' + last.format('YYYY-MM-DD'))
        })
    })
</script>
@endsection


@section('content')
<section class="section">
  <div class="section-header">
    <h1>Semua Absen {{$pegawai->nama}}</h1>
  </div>
  <div class="section-body">
      <div class="row">
      	<div class="col-md-12">
      		<div class="card">
		      <div class="card-body p-0">
		      	<form action="/laporan/absen/{{$pegawai->id}}" method="get">
                    <div class="input-group mb-3 col-md-4 float-right">
                        <input type="text" id="date" name="date" class="form-control">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Filter</button>
                        </div>
                        <a target="_blank" class="btn btn-success" id="exportpdf">Cetak</a>
                    </div>
                </form>
		      	<div class="table-responsive table-invoice">
		      	<table class="table table-striped">
		      		<tbody>
		      			<tr>
		      				<th><h6>No.</h6></th>
		      				<th><h6>Tanggal</h6></th>
							<th><h6>Masuk</h6></th>
							<th><h6>Pulang</h6></th>
							<th><h6>LKH</h6></th>
		      			</tr> 
		      			@php $no = 0; @endphp
		      			@forelse ($data as $abs)
		      			@php $no++; @endphp
							<tr>
								<td>{{$no}}</td>
								<td >{{$user->tglIndo($abs->date)}}</td>
								@if($abs->time_masuk >= '07:30')
                                    <td><div class="badge badge-danger">{{ date('H:i', strtotime($abs->time_masuk)) }}</div></td>
                                @else
                                     <td>{{ date('H:i', strtotime($abs->time_masuk)) }}</td>   
                                @endif
                                
                                @if(is_null($abs->time_pulang))
                                    <td></td>
                                @elseif($abs->time_pulang <= '16:00')
                                     <td><div class="badge badge-warning">{{ date('H:i', strtotime($abs->time_pulang)) }}</div></td>
                                @else
                                    <td>{{ date('H:i', strtotime($abs->time_pulang)) }}</td>
                                @endif
								<td>{{ $abs->lkh}}{{ $abs->id}}</td>
							</tr>
						@empty
                            <tr>
                                <td colspan="4"><center><b>Tidak ada Data Presensi</b></center></td>
                            </tr>
						@endforelse
		      		</tbody>
		      	</table> <!---->
		      </div>
		  </div>
		</div>
	</div>
	</div>
  </div>
</section>
@stop