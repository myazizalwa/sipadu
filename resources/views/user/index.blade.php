@extends('layouts.master')

@section('title')
Admin Sipadu
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Pengelolaan Admin</h1>
  </div>
<div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4>Admin</h4>
                  <div class="card-header-action">
                    <a href="/pengelola/tambah" class="btn btn-success">Tambah Pengelola <i class="fas fa-chevron-right"></i></a>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="table-responsive table-invoice">
                    <table class="table table-striped">
                      <tbody><tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                      </tr>
                      @foreach($user as $us)
                      <tr>
                      	
                        <td><a href="/pengelola/{{$us->id}}/detil">{{$us->name}}</a></td>
                        <td class="font-weight-600">{{$us->email}}</td>
                        @if($us->role == 'admin')
                        <td><div class="badge badge-success">Administrator</div></td>
                        @elseif($us->role == 'keuangan')
                        <td><div class="badge badge-success">Keuangan</div></td>
                        @else
                        <td><div class="badge badge-success">Kepegawaian</div></td>
                        @endif
                        <td>
                          <a href="/pengelola/{{$us->id}}/detil" class="btn btn-primary">Detail</a>
                          @if($us->role != 'admin')
                          <a href="/pengelola/{{$us->id}}/hapus" class="btn btn-danger">Hapus</a>
                          @endif
                        </td>
                        
                      </tr>
                      @endforeach
                    </tbody></table>
                  </div>
                </div>
              </div>
            </div>
          </div>
</section>
@stop