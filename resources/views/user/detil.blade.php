@extends('layouts.master')

@section('title')
Form Tambah Admin
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Form Tambah Admin</h1>
  </div>

  <div class="card">
    <div class="card-header"><h4>Tambah Admin</h4>
    </div> 
      <div class="card-body">
        <form action="/pengelola/{{$admin->id}}/update" method="POST" class="needs-validation" novalidate="" oninput='password2.setCustomValidity(password2.value != password.value ? "Passwords tidak sama." : "")'>
                        {{csrf_field()}} 
          <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label> 
            <div class="col-sm-12 col-md-7">
              <input type="text" placeholder="Full name of the user." class="form-control {{$errors->has('nama') ? 'has-error' : ''}}" name="nama" required="" value="{{$admin->name}}"> 
              <div class="invalid-feedback">
                              Mohon isi nama lengkap
                            </div>
                            @if($errors->has('nama'))
                            <span class="help-block">{{$errors->first('nama')}}</span>
                            {{-- <div class="invalid-feedback">
                                {{$errors->first('nama')}}
                              </div> --}}
                        @endif
            </div>
          </div> 
          <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label> 
            <div class="col-sm-12 col-md-7">
              <input type="text" placeholder="Email address (should be unique)." class="form-control {{$errors->has('email') ? 'has-error' : ''}}" name="email" required="" value="{{$admin->email}}"> <!---->
              <div class="invalid-feedback">
                              Mohon isi email / format tidak sesuai
                            </div>
                            @if($errors->has('email'))
                            <span class="help-block">{{$errors->first('email')}}</span>
                            {{-- <div class="invalid-feedback">
                                {{$errors->first('email')}}
                              </div> --}}
                        @endif
            </div>
          </div> 
          <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Role</label> 
            <div class="col-sm-12 col-md-7">
              <select class="form-control" name="role">
                <option value="kepegawaian" @if($admin->role == 'kepegawaian')  selected @endif >Kepegawaian</option>
                <option value="keuangan" @if($admin->role == 'keuangan')  selected @endif >Keuangan</option>
              </select> <!---->
            </div>
          </div>
          <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Password</label> 
            <div class="col-sm-12 col-md-7">
              <input type="password" autocomplete="new-password" placeholder="Set an account password." class="form-control" name="password" required=""> <!---->
              <div class="invalid-feedback">
                              Mohon isi password
                            </div>
            </div>
          </div> 
          <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Confirm Password</label> 
            <div class="col-sm-12 col-md-7">
              <input type="password" placeholder="Confirm account password." autocomplete="new-password" class="form-control" name="password2" required=""> <!---->
              <div class="invalid-feedback">
                              Password tidak sesuai
                            </div>
            </div>
          </div> 
          <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label> 
            <div class="col-sm-12 col-md-7">
              <button class="btn btn-primary"><span>Update</span></button>
            </div>
          </div>
        </form>
      </div>
    </div>
</section>
@stop