@extends('layouts.master')

@section('title')
Pengelolaan Atasan Langsung
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Pengelolaan Atasan Langsung</h1>
  </div>
<div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4>List Atasan</h4>
                  <div class="card-header-action">
                    <a href="/atasan/tambah" class="btn btn-success">Tambah Atasan <i class="fas fa-chevron-right"></i></a>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="table-responsive table-invoice">
                    <table class="table table-striped">
                      <tbody><tr>
                        <th>Kepala</th>
                        <th>NIP</th>
                        <th>Satuan</th>
                        <th>Action</th>
                      </tr>
                      @foreach($atasan as $atas)
                      <tr>
                      	
                        <td><a href="/atasan/{{$atas->id}}/detil">{{$atas->kepala}}</a></td>
                        <td class="font-weight-600">{{$atas->nip}}</td>
                        <td class="font-weight-600">{{$atas->satuan}}</td>
                        <td>
                          <a href="/atasan/{{$atas->id}}/detil" class="btn btn-primary">Detail</a>
                          <a href="/atasan/{{$atas->id}}/hapus" class="btn btn-danger">Hapus</a>
                        </td>
                        
                      </tr>
                      @endforeach
                    </tbody></table>
                  </div>
                  <div class="card-footer text-left">
                    <nav class="d-inline-block">
                          {{ $atasan->links() }}
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
</section>
@stop