@extends('layouts.master')

@section('title')
Form Tambah Atasan
@endsection

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Form Tambah Atasan</h1>
  </div>

	<div class="card">
		<div class="card-header"><h4>Tambah Atasan</h4>
		</div> 
			<div class="card-body">
				<form action="/atasan/store" method="POST" class="needs-validation" novalidate="">
                        {{csrf_field()}} 
					<div class="form-group row mb-4">
						<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Kepala</label> 
						<div class="col-sm-12 col-md-6">
							<input type="text" placeholder="Nama Lengkap Kepala" class="form-control {{$errors->has('kepala') ? 'has-error' : ''}}" name="kepala" required="" value="{{old('kepala')}}"> 
							<div class="invalid-feedback">
                              Mohon isi nama lengkap
                            </div>
                            @if($errors->has('kepala'))
		                        <span class="help-block">{{$errors->first('kepala')}}</span>
		                    @endif
						</div>
					</div>
					<div class="form-group row mb-4">
						<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP Kepala</label> 
						<div class="col-sm-12 col-md-6">
							<input type="text" placeholder="NIP Kepala" class="form-control {{$errors->has('nip') ? 'has-error' : ''}}" name="nip" required="" value="{{old('nip')}}"> 
							<div class="invalid-feedback">
                              Mohon isi NIP lengkap
                            </div>
                            @if($errors->has('nip'))
		                        <span class="help-block">{{$errors->first('nip')}}</span>
		                    @endif
						</div>
					</div>
					<div class="form-group row mb-4">
						<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Satuan</label> 
						<div class="col-sm-12 col-md-6">
							<input type="text" placeholder="Nama Lengkap Satuan" class="form-control {{$errors->has('satuan') ? 'has-error' : ''}}" name="satuan" required="" value="{{old('satuan')}}"> 
							<div class="invalid-feedback">
                              Mohon isi satuan lengkap
                            </div>
                            @if($errors->has('satuan'))
		                        <span class="help-block">{{$errors->first('satuan')}}</span>
		                    @endif
						</div>
					</div>
					<div class="form-group row mb-4">
						<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label> 
						<div class="col-sm-12 col-md-7">
							<button class="btn btn-primary"><span>Add</span></button>
						</div>
					</div>
				</form>
			</div>
		</div>
</section>
@stop