<?php

use App\Notifications\NotifikasiCuti;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auths.login');
});
Route::get('/register','AuthController@register');
Route::post('/postregister','AuthController@postregister');

Route::get('/login','AuthController@login')->name('login');
Route::post('/postlogin','AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');


Route::group (['middleware' => ['auth','checkRole:admin,kepegawaian,keuangan']], function(){
	Route::get('/dashboard','DashboardController@index');
	Route::get('/pegawai','PegawaiController@index');
	Route::post('/pegawai/tambah','PegawaiController@tambah');
	Route::get('/pegawai/{id}/edit','PegawaiController@edit');
	Route::post('/pegawai/{id}/update','PegawaiController@update');
    Route::get('pegawai/{id}/reset','PegawaiController@reset');
    
	Route::get('pegawai/{id}/hapus','PegawaiController@hapus');
	Route::get('pegawai/{id}/profil','PegawaiController@profil');
	Route::post('pegawai/{id}/addcuti','PegawaiController@addcuti');

	Route::get('/laporan','LaporanController@index');
	Route::get('/laporan/approve','LaporanController@approve');
	Route::get('/laporan/usulan','LaporanController@usulan');
	Route::get('/laporan/reject','LaporanController@reject');
	Route::get('/laporan/pending','LaporanController@pending');

	Route::get('/laporan/{id}/status','CutiController@edit');
	Route::post('/laporan/{id}/update','CutiController@update');
	
	// Laporan Absensi
    Route::get('/laporan/absen/{id}','LaporanController@absen');
    //report absen
	Route::get('/laporan/absen/pdf/{id}/{daterange}', 'LaporanController@absenPdf');
	Route::get('/laporan/presensi','LaporanController@presensi');

    //  end laporan absen route

	Route::get('/pengelola','DashboardController@pengelola');
	Route::get('/pengelola/tambah','DashboardController@tambah');
	Route::post('/pengelola/store','DashboardController@storeadmin');
	Route::get('/pengelola/{id}/detil','DashboardController@detiladmin');
	Route::post('/pengelola/{id}/update','DashboardController@updateadmin');
	Route::get('/pengelola/{id}/hapus','DashboardController@hapusadmin');

	Route::get('/atasan','AtasanController@index');
	Route::get('/atasan/tambah','AtasanController@add');
	Route::post('/atasan/store','AtasanController@store');
	Route::get('/atasan/{id}/detil','AtasanController@detil');
	Route::post('/atasan/{id}/update','AtasanController@update');
	Route::get('/atasan/{id}/hapus','AtasanController@hapus');

	Route::get('/jabatan','JabatanController@index');
	Route::get('/jabatan/tambah','JabatanController@add');
	Route::post('/jabatan/store','JabatanController@store');
	Route::get('/jabatan/{id}/detil','JabatanController@detil');
	Route::post('/jabatan/{id}/update','JabatanController@update');
	Route::get('/jabatan/{id}/hapus','JabatanController@hapus');

	// Hari Libur
	Route::get('/libur','LiburController@index');
});

Route::group (['middleware' => ['auth','checkRole:admin,pegawai']], function(){
	Route::get('/user','DashboardController@user');
	Route::get('/cuti','CutiController@index');
	Route::post('/cuti/tambahcuti','CutiController@tambahcuti');
	Route::get('/cuti/{id}/edit','CutiController@editcuti');
	Route::post('/cuti/{id}/update','CutiController@updatecuti');
	//pengajuan cuti umum
	Route::get('/cuti/umum','CutiController@umum');


	Route::post('/update','PegawaiController@updateUser');
	Route::get('/profilsaya','PegawaiController@profilsaya');
	Route::get('/status/diterima','LaporanController@diterima');

	Route::get('/laporan/{id}/exportPdf','LaporanController@exportPdf');

	// Form Absen
	Route::get('/absen','AbsenController@index');
	Route::post('/absen/masuk','AbsenController@absen');
	
	//report absen
	Route::get('/absen/pdf/{daterange}', 'AbsenController@absenReportPdf')->name('report.absen_pdf');
});
	